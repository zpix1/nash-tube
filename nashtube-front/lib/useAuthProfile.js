import { useEffect, useState } from 'react';
import { getProfile } from '../components/layouts/header/Header';
import { useRouter } from 'next/router';

export const useAuthProfile = (redirect) => {
    const router = useRouter();
    const [loading, setLoading] = useState(false);

    const [profile, setProfile] = useState();
    useEffect(() => {
        (async () => {
            setLoading(true);
            try {
                const profile = await getProfile();
                if (!profile) {
                    if (redirect) {
                        await router.push('/login');
                        return;
                    } else {
                        return {
                            id: 0,
                            username: ''
                        };
                    }
                }
                setProfile(profile);
            } catch (e) {
                console.log(e);
            } finally {
                setLoading(false);
            }
        })();
    }, []);

    return { profile, loading };
};