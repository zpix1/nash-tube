import { useCallback, useEffect, useState } from 'react';
import { BASE_URL } from '../config';

export const useRequest = () => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState();

    const request = useCallback(async ({
                                           type, data, url
                                       }) => {
        setLoading(true);
        try {
            const result = await fetch(`${BASE_URL}${url}`, {
                method: type,
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });
            if (result.status !== 200 || result.redirected) {
                throw new Error(result.statusText);
            }
        } catch (e) {
            console.error(e);
            setError(e);
        } finally {
            setLoading(false);
        }
    }, []);
    return {
        request,
        loading,
        error
    };
};