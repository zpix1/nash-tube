import { BASE_URL } from "../config";
export const fetchJson = async (url, type, data) => {
    const result = await fetch(`${BASE_URL}${url}`, {
        method: type,
        headers: {
            'Content-Type': 'application/json',
        },
        body: type !== 'GET' ? JSON.stringify(data) : undefined
    });
    if (result.status === 403) {
        window.location.replace('/login');
        return;
    }
    if (result.status !== 200 && result.status !== 302) {
        throw new Error(result.statusText || `Error ${result.status}`);
    }
    try {
        return await result.json();
    } catch (e) {
        return undefined;
    }
};
export const fetchMultipart = async (url, type, formData, accept) => {
    const result = await fetch(`${BASE_URL}${url}`, {
        method: type,
        headers: {
            'Accept': '*/*',
            // 'Content-Type': 'multipart/form-data'
        },
        body: formData
    });
    if (result.status !== 200 && result.status !== 302) {
        throw new Error(result.statusText || `Error ${result.status}`);
    }
    return result;
};