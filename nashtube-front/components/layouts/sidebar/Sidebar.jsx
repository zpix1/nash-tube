import style from './Sidebar.module.scss';
import Link from 'next/link';
import {
    CgProfile,
    CgSoftwareDownload,
    CgLogOut,
    CgSoftwareUpload,
    CgLogIn,
    CgBorderAll,
    CgHome, CgList
} from 'react-icons/cg';
import { useRouter } from 'next/router';
import { BASE_URL } from '../../../config';
import { fetchJson } from '../../../lib/fetchJson';

export default function Sidebar({ sidebar, handleToggleSidebar, isLoggedIn }) {
    const router = useRouter();
    const logout = async (e) => {
        e.preventDefault();
        try {
            await fetchJson('/api/auth/logout', 'GET', {});
        } catch (e) {
            console.log(e);
        }
        await router.push('/login');
    };

    return (
        <nav
            className={sidebar ? style.sidebar : style.sidebar_close}
            onClick={() => handleToggleSidebar()}
        >
            {isLoggedIn && <Link href="/profile">
                <li>
                    <a className={style.sideitem}>
                        <CgProfile size={23} />
                        <span>Профиль</span>
                    </a>
                </li>
            </Link>}
            <Link href="/">
                <li>
                    <a className={style.sideitem}>
                        <CgHome size={23} />
                        <span>Все видео</span>
                    </a>
                </li>
            </Link>
            {isLoggedIn && <Link href="/subs">
                <li>
                    <a className={style.sideitem}>
                        <CgList size={23} />
                        <span>Подписки</span>
                    </a>
                </li>
            </Link>}
            {isLoggedIn && <Link href="/upload">
                <li>
                    <a className={style.sideitem}>
                        <CgSoftwareUpload size={23} />
                        <span>Загрузить</span>
                    </a>
                </li>
            </Link>}
            {isLoggedIn && <hr />}
            {isLoggedIn && <li onClick={logout}>
                <a className={style.sideitem}>
                    <CgLogOut size={23} />
                    <span>Выйти</span>
                </a>
            </li>}
            {!isLoggedIn && <Link href="/login">
                <li>
                    <a className={style.sideitem}>
                        <CgLogIn size={23} />
                        <span>Войти</span>
                    </a>
                </li>
            </Link>}
            {isLoggedIn && <hr />}
        </nav>
    );
}
