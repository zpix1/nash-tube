import style from './Header.module.scss';
import Link from 'next/link';
import { FaBars, FaMixer } from 'react-icons/fa';
import { AiOutlineSearch } from 'react-icons/ai';
import { MdNotifications } from 'react-icons/md';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { fetchJson } from '../../../lib/fetchJson';
import { useAuthProfile } from '../../../lib/useAuthProfile';
import { AvatarImage } from '../../elements/avatars/AvatarImage';

export const getProfile = async () => {
    return (await fetchJson('/api/user/self', 'GET', undefined)).user;
};

export default function Header({ handleToggleSidebar, profile, loading }) {
    const router = useRouter();
    const { search_str: base } = router.query;

    const [searchString, setSearchString] = useState(base ?? '');
    useEffect(() => {
        if (base) {
            setSearchString(base);
        }
    }, [base]);
    const [isSidebarOpen, setIsSidebarOpen] = useState(false);

    const writeSearch = (event) => {
        setSearchString(event.target.value);
    };

    const handleSidebar = () => {
        handleToggleSidebar();
        setIsSidebarOpen(!isSidebarOpen);
    };

    return (
        <div className={style.header}>
            <div className={style.header__wrap}>
                <button
                    type="button"
                    className="btn btn-outline-primary"
                    onClick={handleSidebar}
                >
                    {!isSidebarOpen ? (
                        <FaBars className={style.header__menu} size={26} />
                    ) : (
                        <FaMixer className={style.header__menu} size={26} />
                    )}
                </button>

                <Link href="/">
                    <a className={style.header__link}>
                        <img
                            src="https://www.ferra.ru/thumb/1800x0/filters:quality%2875%29:no_upscale%28%29/imgs/2020/12/15/07/4402494/6324d8ac829ed109de5dafa1ab07fce1c9af420d.jpg"
                            alt="logo"
                            className={style.header__logo}
                        />
                        <div>
                            <span>Nash</span>
                            Tube
                        </div>
                    </a>
                </Link>
            </div>

            <form>
                <input
                    type="text"
                    className="form-control"
                    aria-label="Sizing example input"
                    aria-describedby="inputGroup-sizing-default"
                    placeholder="Поиск"
                    value={searchString}
                    onChange={writeSearch}
                />
                <Link
                    href={{
                        pathname: '/search',
                        query: {
                            search_str: searchString.trim().replace(' ', '+'),
                        },
                    }}
                >
                    <a>
                        <button type="submit" className="btn btn-primary">
                            <AiOutlineSearch size={20} />
                        </button>
                    </a>
                </Link>
            </form>
            <div className={style.header__icons}>
                {loading ? (
                    <span
                        className="spinner-border text-danger"
                        role="status"
                        aria-hidden="true"
                    ></span>
                ) : (
                    <>
                        <AvatarImage userId={profile?.id} />
                        <div className={style.header__username}>
                            {profile?.username}
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
