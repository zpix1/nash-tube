import { useQuery } from 'react-query';
import { Container } from 'react-bootstrap';
import Sidebar from './sidebar/Sidebar';
import Header from './header/Header';
import style from '../../styles/app.module.scss';
import { useState } from 'react';
import { useAuthProfile } from '../../lib/useAuthProfile';

export default function MainLayout({ children }) {
    const { loading, profile } = useAuthProfile();

    const [sidebar, toggleSidebar] = useState(false);
    const handleToggleSidebar = () => {
        toggleSidebar((value) => !value);
    };
    return (
        <>
            <Header handleToggleSidebar={handleToggleSidebar}
                    profile={profile}
                    loading={loading}
            />
            <div className={style.app__container}>
                <Sidebar
                    sidebar={sidebar}
                    handleToggleSidebar={handleToggleSidebar}
                    isLoggedIn={Boolean(profile)}
                />
                <Container fluid>{children}</Container>
            </div>
        </>
    );
}
