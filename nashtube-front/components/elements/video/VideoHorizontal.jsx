import { Container, Col, Row } from 'react-bootstrap';
import style from './VideoHorizontal.module.scss';
import Link from 'next/link';
import { BASE_URL } from '../../../config';
import { AvatarImage } from '../avatars/AvatarImage';
import { durationToString } from './Video';
export default function VideoHorizontal({ video, searchScreen }) {
    return (
        <Row className={`${style.videohorizontal} m-2 p-2`}>
            <Col
                xs={6}
                md={searchScreen ? 5 : 6}
                className={style.videohorizontal__left}
            >
                <Link href={`/watch/${video.id}`}>
                    <a className={style.link}>
                        <img
                            src={video.imageUrl}
                            // src={`${BASE_URL}/api/videos/get/preview?id=${video.id}`}
                            alt="preview"
                        />
                        <span>{durationToString(video.duration)}</span>
                    </a>
                </Link>
            </Col>
            <Col
                xs={6}
                md={searchScreen ? 7 : 6}
                className={`${style.videohorizontal__right} p-0`}
            >
                <div
                    className={
                        searchScreen
                            ? `${style.videohorizontal__title_search} mb-2`
                            : `${style.videohorizontal__title} mb-1`
                    }
                >
                    {video.name}
                </div>
                <Link href={`/channel/${video.owner}`}>
                    <a>
                        <div
                            className={
                                searchScreen
                                    ? style.videohorizontal__channel_search
                                    : style.videohorizontal__channel
                            }
                        >
                            <AvatarImage userId={video.owner} />
                            <span>{video.ownerName}</span>
                        </div>
                    </a>
                </Link>
                {searchScreen && (
                    <p className={style.videohorizontal__searchdescr}>
                        Lorem ipsum dolor, sit amet consectetur adipisicing
                        elit. Laudantium at ducimus optio aspernatur officia
                        quidem placeat quo ipsam officiis repudiandae voluptatem
                        numquam rerum deleniti nam sapiente, corporis porro,
                        earum sed? Lorem ipsum dolor sit amet consectetur
                        adipisicing elit. Voluptate enim ipsam natus dolorem ea,
                        ipsa illum molestiae dolores ratione doloremque eum nam
                        inventore adipisci similique repellat, odio cupiditate
                        exercitationem deserunt?
                    </p>
                )}
            </Col>
        </Row>
    );
}
