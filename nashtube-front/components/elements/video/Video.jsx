import style from './Video.module.scss';
import Link from 'next/link';
import { BASE_URL } from '../../../config';
import { AvatarImage } from '../avatars/AvatarImage';
import { formatDistance, intervalToDuration } from 'date-fns';
import { ru } from 'date-fns/locale';

export const durationToString = (duration) => formatDistance(0, duration, { includeSeconds: true, locale: ru });

export default function Video({ video, channelScreen }) {
    return (
        <div className={style.video}>
            <Link href={`/watch/${video.id}`}>
                <a>
                    <div className={style.video__top}>
                        <img
                            // src={`${BASE_URL}/api/videos/get/preview?id=${video.id}`}
                            src={video.imageUrl}
                            alt="preview"
                        />
                        <span>{durationToString(video.duration)}</span>
                    </div>
                    <div className={style.video__title}>{video.name}</div>
                </a>
            </Link>
            {!channelScreen && (
                <Link href={`/channel/${video.owner}`}>
                    <a>
                        <div className={style.video__channel}>
                            <AvatarImage
                                userId={video.owner}
                            />
                            <p>{video.ownerName}</p>
                        </div>
                    </a>
                </Link>
            )}
        </div>
    );
}
