import { useEffect, useState } from 'react';
import { BASE_URL } from '../../../config';

const DEFAULT_IMAGE = "https://firefoxusercontent.com/00000000000000000000000000000000";

export const AvatarImage = ({ userId }) => {
    const [useDefault, setUseDefault] = useState(false);
    useEffect(() => {
        if (userId) {
            setUseDefault(false);
        }
    }, [userId]);
    if (!userId) {
        return <img
            src={DEFAULT_IMAGE}
            alt="profileAvatar"
        />;
    }
    return <img
        src={useDefault ? DEFAULT_IMAGE : `${BASE_URL}/api/user/avatar?id=${userId}`}
        onError={() => setUseDefault(true)}
        alt="profileAvatar"
    />;
};