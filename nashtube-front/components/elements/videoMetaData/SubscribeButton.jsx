import style from './VideoMetaData.module.scss';
import { fetchJson } from '../../../lib/fetchJson';

const subscribeChannelById = async (channelId) => {
    return await fetchJson(
        `/api/channels/subscribe/subscribe?id=${channelId}`,
        'POST'
    );
};

export const SubscribeButton = ({
    profile,
    isSubscribed,
    subscribers,
    channelId,
    reloadMetadata,
}) => {
    const subscribe = async () => {
        try {
            await subscribeChannelById(channelId);
            await reloadMetadata();
        } catch (e) {
            console.error(e);
        }
    };
    return (
        <>
            {profile &&
                channelId != profile?.id &&
                (isSubscribed ? (
                    <button
                        className={style.videometa__subscribed}
                        onClick={subscribe}
                    >
                        Вы подписаны
                    </button>
                ) : (
                    <button
                        className={style.videometa__btn}
                        onClick={subscribe}
                    >
                        Подписка
                    </button>
                ))}
        </>
    );
};
