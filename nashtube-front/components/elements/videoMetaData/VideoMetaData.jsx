import style from './VideoMetaData.module.scss';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { MdThumbUp, MdThumbDown } from 'react-icons/md';
import { fetchJson } from '../../../lib/fetchJson';
import { useAuthProfile } from '../../../lib/useAuthProfile';
import { spragat } from '../../../lib/spragat';
import { SubscribeButton } from './SubscribeButton';
import { AvatarImage } from '../avatars/AvatarImage';

const deleteVideoFromChannelById = async (channelId, videoId) => {
    return await fetchJson(
        `/api/channel/${channelId}/videos/delete?vId=${videoId}`,
        'DELETE',
        {}
    );
};

const likeVideoById = async (videoId) => {
    return await fetchJson(`/api/videos/like?id=${videoId}`, 'POST', {
        id: videoId
    });
};

const dislikeVideoById = async (videoId) => {
    return await fetchJson(`/api/videos/dislike?id=${videoId}`, 'POST');
};

export default function VideoMetaData({ video, reloadMetadata }) {
    const router = useRouter();
    const { profile } = useAuthProfile();

    const deleteVideo = async () => {
        try {
            await deleteVideoFromChannelById(video.owner, video.id);
            await router.replace('/');
        } catch (e) {
            console.error(e);
        }
    };

    const like = async () => {
        try {
            await likeVideoById(video.id);
            await reloadMetadata();
        } catch (e) {
            console.error(e);
        }
    };

    const dislike = async () => {
        try {
            await dislikeVideoById(video.id);
            await reloadMetadata();
        } catch (e) {
            console.error(e);
        }
    };

    return (
        <div className="videometa py-2">
            <div className={style.videometa__top}>
                <div className="d-flex justify-content-between align-items-center py-1">
                    <h5>{video.name}</h5>
                    <div>
                        <span
                            className={style.videometa__thumb}
                            onClick={like}
                            style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                                gap: '6px',
                                color: video.liked ? '#0c58c9' : '',
                                pointerEvents: profile ? 'inherit' : 'none'
                            }}
                        >
                            <MdThumbUp size={26} /> {video.likes}
                        </span>
                        <span
                            className={style.videometa__thumb}
                            onClick={dislike}
                            style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                                gap: '6px',
                                color: video.disliked ? '#d01234' : '',
                                pointerEvents: profile ? 'inherit' : 'none'
                            }}
                        >
                            <MdThumbDown size={26} /> {video.dislikes}
                        </span>
                    </div>
                </div>
            </div>
            <div
                className={`${style.videometa__channel} d-flex justify-content-between align-items-center my-2 py-3`}
            >
                <Link href={`/channel/${video.owner}`}>
                    <a>
                        <div className={style.videometa__channel__link}>
                            <AvatarImage
                                userId={video.owner}
                            />
                            <div className="d-flex flex-column justify-content-between">
                                <span>{video.ownerName}</span>
                                <span
                                    style={{
                                        fontSize: '0.9rem',
                                        top: '0',
                                        color: 'rgb(28, 28, 125)'
                                    }}
                                >
                                    {video.subscribers}{' '}
                                    {spragat(
                                        video.subscribers,
                                        'подписчик',
                                        'подписчика',
                                        'подписчиков'
                                    )}
                                </span>
                            </div>
                        </div>
                    </a>
                </Link>

                <SubscribeButton
                    subscribers={video.subscribers}
                    reloadMetadata={reloadMetadata}
                    profile={profile}
                    isSubscribed={video.subscribedAt}
                    channelId={video.owner}
                />
            </div>
            <div className={style.videometa__description}>
                {video.description}
            </div>
            {video.owner === profile?.id && (
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        margin: '10px 0'
                    }}
                >
                    <button
                        className={style.videometa__delete}
                        onClick={deleteVideo}
                    >
                        Удалить
                    </button>
                </div>
            )}
        </div>
    );
}
