import style from './Comment.module.scss';
import Link from 'next/link';
import { AvatarImage } from '../avatars/AvatarImage';
import { format, formatDistance, formatRelative, subDays } from 'date-fns';
import { parseFromTimeZone } from 'date-fns-timezone';
import { ru } from 'date-fns/locale';

export default function Comment({ comment, deleteComment }) {
    return (
        <div className={style.comment}>
            <Link href={`/channel/${comment.uid}`}>
                <a>
                    <AvatarImage userId={comment.uid} />
                </a>
            </Link>
            <div className={style.comment__body}>
                <p className={style.comment__header}>
                    <span className={style.comment__username}>
                        {comment.channelName}
                    </span>{' '}
                    •{' '}
                    <span className={style.comment__date}>{
                        formatRelative(
                            parseFromTimeZone(comment.date, { timeZone: 'UTC' }),
                            new Date(), { locale: ru })
                    }</span>
                    {deleteComment && <span className={style.comment__delete} onClick={deleteComment}>
                        Удалить
                    </span>}
                </p>
                <p className={style.comment__text}>{comment.text}</p>
            </div>
        </div>
    );
}
