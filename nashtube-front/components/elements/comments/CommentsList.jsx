import style from './CommentsList.module.scss';
import Comment from './Comment';
import { useState } from 'react';
import { MdSend } from 'react-icons/md';
import { AvatarImage } from '../avatars/AvatarImage';

export default function CommentsList({ profileId, comments, submitComment, deleteComment }) {
    const [yourComment, setYourComment] = useState('');
    const sendComment = (e) => {
        e.preventDefault();
        submitComment(yourComment);
        setYourComment('');
    };
    return (
        <div className={style.comments}>
            <p style={{ color: 'rgb(28, 28, 125)', fontWeight: '700' }}>
                {'Комментариев:  ' + comments?.length}
            </p>
            {submitComment && <div className={`${style.comments__form}`}>
                <AvatarImage userId={profileId} />
                <form onSubmit={sendComment}>
                    <div className="input-group mb-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Оставьте комментарий"
                            value={yourComment}
                            onChange={(e) => setYourComment(e.target.value)}
                        />
                        <div className="input-group-append">
                            <button
                                type="submit"
                                className="btn btn-outline-primary"
                            >
                                <MdSend size={25} />
                            </button>
                        </div>
                    </div>
                </form>
            </div>}
            <div className={style.comments__list}>
                {comments?.map((comment) => (
                    <Comment key={comment.id} comment={comment}
                             deleteComment={profileId === comment.uid && (() => deleteComment(comment.id))} />
                ))}
            </div>
        </div>
    );
}
