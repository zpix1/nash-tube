import style from './UploadForm.module.scss';
import { useForm } from 'react-hook-form';
import { Form } from 'react-bootstrap';
import { ImFileVideo, ImImages } from 'react-icons/im';
import { FcApproval } from 'react-icons/fc';
import Dropzone from 'react-dropzone';
import axios from 'axios';
import { useState } from 'react';
import { BASE_URL } from '../../../config';
import { fetchJson, fetchMultipart } from '../../../lib/fetchJson';
import { getProfile } from '../../layouts/header/Header';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useAuthProfile } from '../../../lib/useAuthProfile';

const getVideoDuration = file =>
    new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
            const media = new Audio(reader.result);
            media.onloadedmetadata = () => resolve(media.duration);
        };
        reader.readAsDataURL(file);
        reader.onerror = error => reject(error);
    });

export default function UploadForm() {
    const {
        register,
        setError,
        formState: { errors, isValid },
        handleSubmit,
        reset
    } = useForm({
        mode: 'onChange'
    });
    const router = useRouter();
    const [videoFile, setVideo] = useState(null);
    const [imageFile, setImage] = useState(null);
    const [isVideoLoading, setIsVideoLoading] = useState(false);
    const [isImageLoading, setIsImageLoading] = useState(false);
    const acceptVideo = (files) => {
        setVideo(files[0]);
    };
    const acceptImage = (files) => {
        setImage(files[0]);
    };
    const acceptVideoByInput = (event) => {
        setVideo(event.target.files[0]);
    };
    const acceptImageByInput = (event) => {
        setImage(event.target.files[0]);
    };
    const [loading, setLoading] = useState(false);
    const { profile } = useAuthProfile(true);

    const sendForm = async (channelId, formData) => {
        setLoading(true);
        try {
            const duration = await getVideoDuration(formData.get('video'));
            const result = await fetchJson(
                `/api/channel/${profile.id}/videos/upload`,
                'POST',
                {
                    name: formData.get('name'),
                    description: formData.get('description'),
                    duration: Math.ceil(duration * 1000)
                }
            );
            setLoading(false);

            const imageUpload = async () => {
                setIsImageLoading(true);
                return await fetch(result.imageUrl, {
                    method: 'PUT',
                    body: formData.get('image'),
                    headers: { 'Content-Type': 'image/*', 'Cache-Control': 'max-age=200' }
                });
            };
            const videoUpload = async () => {
                setIsImageLoading(true);
                return await fetch(result.videoUrl, {
                    method: 'PUT',
                    body: formData.get('video'),
                    headers: { 'Content-Type': 'video/mp4', 'Cache-Control': 'max-age=200' }
                });
            };

            const uploadRes = await Promise.all([videoUpload(), imageUpload()]);

            setVideo(null);
            setImage(null);
            reset();
        } catch (e) {
            setError('name', {
                message: e.message
            });
            console.error(e);
        } finally {
            setLoading(false);
            setIsVideoLoading(false);
            setIsImageLoading(false);
        }
    };

    const onSubmit = async (data) => {
        if (!videoFile) {
            setError('selectedVideo', {
                type: 'emptyfile',
                message: 'Видеофайл не указан'
            });
            return;
        }
        if (!imageFile) {
            setError('selectedImage', {
                type: 'emptyfile',
                message: 'Превью не указано'
            });
            return;
        }
        if (videoFile.type !== 'video/mp4') {
            setError('selectedVideo', {
                type: 'filetype',
                message: 'Доступны только форматы mp4'
            });
            return;
        }
        const formData = new FormData();

        formData.append('video', videoFile);
        formData.append('image', imageFile);
        formData.append('name', data.name);
        formData.append('description', data.description);

        await sendForm(1, formData);
    };

    return (
        <div
            className={`row d-flex justify-content-center align-items-center h-100 ${style.uploadform}`}
        >
            <div className="col-12 col-md-8 col-lg-6 col-xl-6">
                <Form
                    onSubmit={handleSubmit(onSubmit)}
                    className={`p-5 text-center ${style.uploadform__form}`}
                >
                    <h3 className={`mb-4 ${style.uploadform__title}`}>
                        Загрузка видео
                    </h3>
                    <div className={style.uploadform__descr}>
                        Здесь вы можете загрузить свое видео. Не забудьте
                        указать превью, название и описание.
                    </div>
                    <div className="form-outline mb-2">
                        <div className={style.uploadform__dropwrap}>
                            <Dropzone onDrop={acceptVideo} multiple={false}>
                                {({ getRootProps, getInputProps }) => (
                                    <section>
                                        <div
                                            className={
                                                videoFile
                                                    ? style.uploadform__dropzone +
                                                    ' ' +
                                                    style.uploadform__dropzone_videoapproval
                                                    : style.uploadform__dropzone
                                            }
                                            {...getRootProps()}
                                        >
                                            <input
                                                {...getInputProps()}
                                                {...register('selectedVideo')}
                                            />
                                            {videoFile ? null : (
                                                <ImFileVideo size={40} />
                                            )}
                                        </div>
                                        <div
                                            className={
                                                style.uploadform__errormsg
                                            }
                                        >
                                            {errors?.selectedVideo && (
                                                <p>
                                                    {errors?.selectedVideo
                                                        ?.message || 'Error'}
                                                </p>
                                            )}
                                        </div>
                                    </section>
                                )}
                            </Dropzone>
                            <Dropzone onDrop={acceptImage} multiple={false}>
                                {({ getRootProps, getInputProps }) => (
                                    <section>
                                        <div
                                            className={
                                                imageFile
                                                    ? style.uploadform__dropzone +
                                                    ' ' +
                                                    style.uploadform__dropzone_imgapproval
                                                    : style.uploadform__dropzone
                                            }
                                            {...getRootProps()}
                                        >
                                            <input
                                                {...getInputProps()}
                                                {...register('selectedImage')}
                                            />
                                            {videoFile ? null : (
                                                <ImImages size={40} />
                                            )}
                                        </div>
                                        <div
                                            className={
                                                style.uploadform__errormsg
                                            }
                                        >
                                            {errors?.selectedImage && (
                                                <p>
                                                    {errors?.selectedImage
                                                        ?.message || 'Error'}
                                                </p>
                                            )}
                                        </div>
                                    </section>
                                )}
                            </Dropzone>
                        </div>
                        <div className={style.uploadform__fileinputwrap}>
                            <Form.Group className={style.uploadform__fileinput}>
                                <Form.Control
                                    type="file"
                                    size="sm"
                                    onChange={acceptVideoByInput}
                                />
                                {isVideoLoading && (
                                    <span
                                        className="spinner-border spinner-border-sm"
                                        role="status"
                                        aria-hidden="true"
                                    ></span>
                                )}
                            </Form.Group>
                            <Form.Group className={style.uploadform__fileinput}>
                                {isImageLoading && (
                                    <span
                                        className="spinner-border spinner-border-sm"
                                        role="status"
                                        aria-hidden="true"
                                    ></span>
                                )}
                                <Form.Control
                                    type="file"
                                    size="sm"
                                    onChange={acceptImageByInput}
                                />
                            </Form.Group>
                        </div>
                        <label className="form-label">
                            <h5>Название</h5>
                        </label>
                        <input
                            {...register('name', {
                                required: 'Необходимо указать название видео',
                                minLength: {
                                    value: 3,
                                    message:
                                        'Название должно содержать минимум 3 символа'
                                },
                                maxLength: {
                                    value: 100,
                                    message:
                                        'Название может содержать максимум 100 символов'
                                }
                            })}
                            type="text"
                            className="form-control form-control-lg"
                        />
                        <div className={style.uploadform__errormsg}>
                            {errors?.name && <p>{errors?.name?.message}</p>}
                        </div>
                    </div>

                    <div className="form-outline mb-2">
                        <label className="form-label">
                            <h5>Описание</h5>
                        </label>
                        <textarea
                            {...register('description', {
                                required: 'Необходимо указать описание',
                                minLength: {
                                    value: 5,
                                    message:
                                        'Описание должно содержать минимум 5 символов'
                                }
                            })}
                            type="text"
                            className="form-control"
                        />
                        <div className={style.uploadform__errormsg}>
                            {errors?.description && (
                                <p>{errors?.description?.message}</p>
                            )}
                        </div>
                    </div>

                    <div className={style.btnwrap}>
                        <button
                            className="btn btn-outline-primary btn-lg btn-block"
                            type="submit"
                            disabled={loading || isVideoLoading || isImageLoading}
                        >
                            Загрузить
                            {loading && (
                                <span
                                    className="spinner-border spinner-border-sm"
                                    role="status"
                                    aria-hidden="true"
                                ></span>
                            )}
                        </button>
                    </div>
                </Form>
            </div>
        </div>
    );
}
