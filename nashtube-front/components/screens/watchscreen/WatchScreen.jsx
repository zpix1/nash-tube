import { Container, Col, Row } from 'react-bootstrap';
import style from './WatchScreen.module.scss';
import VideoHorizontal from '../../elements/video/VideoHorizontal';
import VideoMetaData from '../../elements/videoMetaData/VideoMetaData';
import CommentsList from '../../elements/comments/CommentsList';
import { useEffect, useMemo, useState } from 'react';
import { fetchJson } from '../../../lib/fetchJson';
import { BASE_URL } from '../../../config';
import { getRandomVideos } from '../homescreen/HomeScreen';
import { RuLoader } from '../homescreen/RuLoader';
import { useAuthProfile } from '../../../lib/useAuthProfile';

const getVideoById = async (id) => {
    return await fetchJson(`/api/videos/get/data?id=${id}`, 'GET', undefined);
};

const getCommentsByVideoId = async (id) => {
    return await fetchJson(`/api/comment/get?id=${id}`, 'GET', undefined);
};

const submitCommentById = async (commentText, userId, videoId) => {
    return await fetchJson(`/api/comment/add`, 'POST', {
        uId: userId,
        vId: videoId,
        text: commentText
    });
};

const deleteCommentById = async (commentId) => {
    return await fetchJson(`/api/comment/delete?id=${commentId}`, 'DELETE', {
        id: commentId
    });
};

export default function WatchScreen({ id }) {
    const { profile } = useAuthProfile();
    const [video, setVideo] = useState();
    const [recs, setRecs] = useState();
    const [comments, setComments] = useState();
    const videoUrl = useMemo(() => video?.videoUrl, [video?.id]);

    const submitComment = async (text) => {
        if (!profile) return;
        if (!id) return;
        try {
            await submitCommentById(text, profile.id, id);
            loadComments();
        } catch (e) {
            console.error(e);
        }
    };

    const deleteComment = async (commentId) => {
        if (!profile) return;
        if (!id) return;
        try {
            await deleteCommentById(commentId);
            loadComments();
        } catch (e) {
            console.error(e);
        }
    }

    const loadComments = () => {
        if (!id) return;
        (async () => {
            try {
                const comments = await getCommentsByVideoId(id);
                comments.sort((a, b) => new Date(b.date) - new Date(a.date));
                setComments(comments);
            } catch (e) {
                console.error(e);
            }
        })();
    };

    const loadVideo = () => {
        if (!id) return;
        (async () => {
            try {
                setVideo(await getVideoById(id));
            } catch (e) {
                console.error(e);
            }
        })();
    };

    const loadRecs = () => {
        if (!id) return;
        (async () => {
            try {
                const randomVideos = await getRandomVideos();
                setRecs(
                    randomVideos
                        .filter((rec) => rec.id.toString() !== id)
                        .slice(0, 5)
                );
            } catch (e) {
                console.error(e);
            }
        })();
    };

    useEffect(() => {
        loadVideo();
        loadRecs();
        loadComments();
    }, [id]);

    return (
        <Container>
            <Row>
                <Col lg={8}>
                    {video && (
                        <div className={style.watchscreen__player}>
                            <video
                                src={videoUrl}
                                // src={`${BASE_URL}/api/videos/get/stream?id=${id}`}
                                title="NashTube video player"
                                width="100%"
                                height="100%"
                                controls="controls"
                                autoPlay
                            ></video>
                        </div>
                    )}
                    {video && (
                        <VideoMetaData
                            channelID={0}
                            video={video}
                            reloadMetadata={loadVideo}
                        />
                    )}
                    {comments ? <CommentsList
                        profileId={profile?.id}
                        deleteComment={deleteComment}
                        comments={comments}
                        submitComment={profile && submitComment}
                    /> : <RuLoader />}
                </Col>
                <Col lg={4}>
                    {recs?.map((v) => (
                        <VideoHorizontal key={v.id} video={v} />
                    ))}
                </Col>
            </Row>
        </Container>
    );
}
