import { Container, Col, Row, Form } from 'react-bootstrap';
import Video from '../../elements/video/Video';
import style from './ProfileScreen.module.scss';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { getProfile } from '../../layouts/header/Header';
import { fetchJson, fetchMultipart } from '../../../lib/fetchJson';
import { useAuthProfile } from '../../../lib/useAuthProfile';
import { spragat } from '../../../lib/spragat';
import { AvatarImage } from '../../elements/avatars/AvatarImage';

export const getUserVideos = async (id) => {
    return await fetchJson(`/api/user/channel?id=${id}`, 'GET', undefined);
};

export const getSubs = async () => {
    return await fetchJson(`/api/channels/subscribe/get`, 'POST', undefined);
};

export default function ProfileScreen(props) {
    const { profile } = useAuthProfile(true);
    const [loading, setLoading] = useState(true);
    const [videos, setVideos] = useState();
    const [subs, setSubs] = useState();
    const [uploaderVisible, setUploaderVisible] = useState(false);
    const [avatarFile, setAvatarFile] = useState(null);
    const [avatarEmpty, setAvatarEmpty] = useState(false);

    const acceptAvatar = (event) => {
        setAvatarFile(event.target.files[0]);
        setAvatarEmpty(false);
    };

    const uploadAvatar = async () => {
        if (!avatarFile) {
            setAvatarEmpty(true);
            return;
        }

        try {
            const formData = new FormData();
            formData.append('file', avatarFile);

            await fetchMultipart(
                '/api/user/change_avatar',
                'POST',
                formData
            );

            location.reload();
        } catch (e) {
            console.error(e);
        }
        //здесь слать
        console.log(avatarFile);
    };

    const uploader = (
        <>
            <Form.Group className={style.profile__upload}>
                <Form.Control
                    type="file"
                    className={style.profile__upload_input}
                    onChange={acceptAvatar}
                />
            </Form.Group>
            {avatarEmpty && (
                <div style={{ marginTop: '5px', color: 'red' }}>
                    Вы не указали файл
                </div>
            )}
            <button
                className={`btn btn-outline-primary ${style.profile__upload_btn}`}
                onClick={uploadAvatar}
            >
                Загрузить
            </button>
        </>
    );

    useEffect(() => {
        if (profile) {
            (async () => {
                setLoading(true);
                try {
                    setVideos((await getUserVideos(profile.id)).videos);
                    setSubs(await getSubs());
                } catch (e) {
                    console.log(e);
                } finally {
                    setLoading(false);
                }
            })();
        }
    }, [profile?.id]);

    return (
        <Container className={style.profile}>
            <Row>
                <div className={`px-5 py-2 my-2 ${style.profile__head}`}>
                    <div className={style.profile__details}>
                        <h5>Добро пожаловать,</h5>
                        <div className={style.profile__details__name}>
                            {profile?.username ?? '...'}
                        </div>
                        <div className={style.profile__portrait}>
                            <AvatarImage userId={profile?.id} />
                            <div
                                className={style.profile__portrait_overlay}
                            ></div>
                            <button
                                className={`btn btn-outline-light ${style.profile__portrait_changebtn}`}
                                onClick={() =>
                                    setUploaderVisible(!uploaderVisible)
                                }
                            >
                                {!uploaderVisible ? 'Сменить' : 'Скрыть'}
                            </button>
                        </div>
                        {uploaderVisible && uploader}

                        <div className={style.profile__details__sub}>
                            Вы подписаны на {subs?.length}{' '}
                            {spragat(
                                subs?.length,
                                'пользователя',
                                'пользователей',
                                'пользователей'
                            )}
                        </div>
                        <div className={style.profile__details__sub}>
                            У вас {profile?.subscribers}{' '}
                            {spragat(
                                profile?.subscribers,
                                'подписчик',
                                'подписчика',
                                'подписчиков'
                            )}
                        </div>
                    </div>
                </div>
            </Row>
            <Row>
                <h1 className={style.profile__descr}>Ваши видео</h1>
                {videos?.map((v, i) => (
                    <Col key={i} lg={3} md={3}>
                        <Video video={v} channelScreen />
                    </Col>
                ))}
            </Row>
        </Container>
    );
}
