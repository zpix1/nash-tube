import { Container, Col, Row } from 'react-bootstrap';
import { BiRightArrow } from 'react-icons/bi';
import Video from '../../elements/video/Video';
import { useEffect, useState } from 'react';
import { fetchJson, fetchMultipart } from '../../../lib/fetchJson';
import { useRouter } from 'next/router';
import { RuLoader } from './RuLoader';
import style from '../homescreen/HomeScreen.module.scss';


export const getRandomVideos = async () => {
    return await fetchJson('/api/videos/get/random', 'GET', {});
};

export default function HomeScreen(props) {
    const [loading, setLoading] = useState(true);
    const [videos, setVideos] = useState([]);
    const [error, setError] = useState();

    useEffect(() => {
        (async () => {
            setLoading(true);
            try {
                setVideos(await getRandomVideos());
            } catch (e) {
                setError('Ошибка загрузки видео');
                console.error(e);
            } finally {
                setLoading(false);
            }
        })();
    }, []);

    const content = (() => {
        if (loading) {
            return <RuLoader />;
        }
        if (error) {
            return (
                <div
                    style={{
                        color: 'red',
                        fontSize: '30px',
                        textAlign: 'center',
                        padding: 50,
                    }}
                >
                    {error}
                </div>
            );
        }
        if (videos.length === 0) {
            return (
                <div
                    style={{
                        fontSize: '30px',
                        textAlign: 'center',
                        padding: 50,
                    }}
                >
                    Видео еще нет
                </div>
            );
        }
        return videos.map((v) => (
            <Col key={v.id.toString()} lg={3} md={4}>
                <Video video={v} />
            </Col>
        ));
    })();

    return (
        <Container>
            <div className={style.title}>Все видео</div>
            <Row>{content}</Row>
        </Container>
    );
}
