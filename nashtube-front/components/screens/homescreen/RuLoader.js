import style from './HomeScreen.module.scss';
import { BiRightArrow } from 'react-icons/bi';

export const RuLoader = () => {
    return <div className={style.loaders}>
        <div className="spinner-grow text-secondary"></div>
        <div className="spinner-grow text-primary">
                            <span className="sr-only">
                                <BiRightArrow size={40} />
                            </span>
        </div>
        <div className="spinner-grow text-danger"></div>
    </div>
}