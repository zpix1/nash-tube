import { Container, Col, Row } from 'react-bootstrap';
import Video from '../../elements/video/Video';
import style from './ChannelScreen.module.scss';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { getProfile } from '../../layouts/header/Header';
import { fetchJson } from '../../../lib/fetchJson';
import { spragat } from '../../../lib/spragat';
import { SubscribeButton } from '../../elements/videoMetaData/SubscribeButton';
import { useAuthProfile } from '../../../lib/useAuthProfile';
import { AvatarImage } from '../../elements/avatars/AvatarImage';
import { getUserVideos } from '../profilescreen/ProfileScreen';

export const getUserById = async (id) => {
    return await fetchJson(`/api/user/get?id=${id}`, 'GET', undefined);
};

export default function ChannelScreen({ id }) {
    const router = useRouter();
    const {profile} = useAuthProfile();
    const [loading, setLoading] = useState(true);
    const [user, setUser] = useState();
    const [videos, setVideos] = useState();

    const loadUser = async () => {
        setLoading(true);
        try {
            const userData = await getUserById(id);
            setUser(userData);
            setVideos((await getUserVideos(userData.id)).videos);
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        if (id) {
            loadUser();
        }
    }, [id]);

    return (
        <Container className={style.channel}>
            <Row>
                <div
                    className={`px-5 py-2 my-2 d-flex justify-content-between align-items-center ${style.channel__head}`}
                >
                    <div
                        className={`d-flex align-items-center ${style.channel__details}`}
                    >
                        <AvatarImage
                            userId={user?.id}
                        />
                        <div className={style.channel__info}>
                            <div className={style.channel__details__name}>
                                {user?.name ?? '...'}
                            </div>
                            <span>{user?.subscribers} {spragat(user?.subscribers, 'подписчик', 'подписчика', 'подписчиков')}</span>
                        </div>
                    </div>
                    {user && <SubscribeButton
                        profile={profile}
                        subscribers={user.subscribers}
                        isSubscribed={user.subscribedAt}
                        reloadMetadata={loadUser}
                        channelId={id}
                    />}
                </div>
            </Row>
            <Row>
                {videos?.map((v, i) => (
                    <Col key={i} lg={3} md={3}>
                        <Video video={v} channelScreen />
                    </Col>
                ))}
            </Row>
        </Container>
    );
}
