import style from './Auth.module.scss';
import { useQuery } from 'react-query';
import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { fetchJson } from '../../../lib/fetchJson';
import { useRouter } from 'next/router';
export default function SignUpForm({ switchForm }) {
    const {
        register,
        watch,
        formState: { errors, isValid },
        handleSubmit,
        reset,
        setError,
    } = useForm({
        mode: 'onChange',
    });

    const [loading, setLoading] = useState(false);
    const router = useRouter();

    const onSubmit = async (data) => {
        setLoading(true);
        try {
            const result = await fetchJson(`/api/auth/register`, 'POST', {
                username: data.email,
                password: data.password,
            });
            switchForm();
            return;
        } catch (e) {
            setError('password', {
                message: 'Неверный логин или пароль',
            });
            console.error(e);
        } finally {
            setLoading(false);
        }
    };

    return (
        <section className={`vh-100 ${style.authpage}`}>
            <div className="container py-5 h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div className={`card ${style.authpage__card}`}>
                            <form
                                onSubmit={handleSubmit(onSubmit)}
                                className={`card-body p-5 text-center ${style.authpage__form}`}
                            >
                                <h3 className="mb-3">Регистрация</h3>

                                <div className="form-outline mb-3">
                                    <label className="form-label">
                                        <h5>Почта</h5>
                                    </label>
                                    <input
                                        {...register('email', {
                                            required:
                                                'Необходимо указать email',
                                            // pattern: {
                                            //     value: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                            //     message:
                                            //         'Неверный формат email',
                                            // },
                                        })}
                                        type="email"
                                        className="form-control form-control-lg"
                                    />
                                    <div className={style.authpage__errormsg}>
                                        {errors?.email && (
                                            <p>{errors?.email?.message}</p>
                                        )}
                                    </div>
                                </div>

                                <div className="form-outline mb-3">
                                    <label className="form-label">
                                        <h5>Пароль</h5>
                                    </label>
                                    <input
                                        {...register('password', {
                                            required:
                                                'Необходимо указать пароль',
                                            minLength: {
                                                value: 1,
                                                message:
                                                    'Пароль должен содержать минимум 1 символов',
                                            },
                                        })}
                                        type="password"
                                        className="form-control form-control-lg"
                                    />
                                    <div className={style.authpage__errormsg}>
                                        {errors?.password && (
                                            <p>{errors?.password?.message}</p>
                                        )}
                                    </div>
                                </div>

                                <div className="form-outline mb-3">
                                    <label className="form-label">
                                        <h5>Подтвердите пароль</h5>
                                    </label>
                                    <input
                                        {...register('confirmPassword', {
                                            required:
                                                'Необходимо подтвердить пароль',
                                            validate: (val) => {
                                                if (watch('password') != val) {
                                                    return 'Пароли не совпадают';
                                                }
                                            },
                                        })}
                                        type="password"
                                        className="form-control form-control-lg"
                                    />
                                    <div className={style.authpage__errormsg}>
                                        {errors?.confirmPassword && (
                                            <p>
                                                {
                                                    errors?.confirmPassword
                                                        ?.message
                                                }
                                            </p>
                                        )}
                                    </div>
                                </div>

                                <div className={style.btnwrap}>
                                    <hr />
                                    <button
                                        className="btn btn-primary btn-lg btn-block"
                                        type="submit"
                                        disabled={loading}
                                    >
                                        Зарегистрироваться
                                        {loading && (
                                            <span
                                                className="spinner-border spinner-border-sm"
                                                role="status"
                                                aria-hidden="true"
                                            ></span>
                                        )}
                                    </button>
                                    <a
                                        className={style.authpage__link}
                                        type="submit"
                                        onClick={(e) => switchForm(e)}
                                    >
                                        Уже зарегистрированы?
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
