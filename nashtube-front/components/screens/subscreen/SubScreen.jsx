import { Container, Col, Row } from 'react-bootstrap';
import { BiRightArrow } from 'react-icons/bi';
import Video from '../../elements/video/Video';
import { useEffect, useState } from 'react';
import { fetchJson, fetchMultipart } from '../../../lib/fetchJson';
import style from '../homescreen/HomeScreen.module.scss';
import { useRouter } from 'next/router';
import { RuLoader } from '../homescreen/RuLoader';
import { spragat } from '../../../lib/spragat';

export const getSubVideos = async () => {
    return await fetchJson('/api/channels/subscribe/videos', 'GET', {});
};

export default function SubScreen() {
    const [loading, setLoading] = useState(true);
    const [videos, setVideos] = useState([]);
    const [error, setError] = useState();

    useEffect(() => {
        (async () => {
            setLoading(true);
            try {
                setVideos(await getSubVideos());
            } catch (e) {
                setError('Ошибка загрузки видео');
                console.error(e);
            } finally {
                setLoading(false);
            }
        })();
    }, []);

    const content = (() => {
        if (loading) {
            return <RuLoader />;
        }
        if (error) {
            return (
                <div
                    style={{
                        color: 'red',
                        fontSize: '30px',
                        textAlign: 'center',
                        padding: 50
                    }}
                >
                    {error}
                </div>
            );
        }
        if (videos.length === 0) {
            return (
                <div
                    style={{
                        fontSize: '30px',
                        textAlign: 'center',
                        padding: 50
                    }}
                >
                    Видео еще нет
                </div>
            );
        }
        return Array.from(new Set(videos.map(v => v.owner))).map(
            o => {
                const ownerVideos = videos.filter(({ owner }) => owner === o);
                return <>
                    <div
                        className={style.subtitle}>{ownerVideos[0].ownerName} ({ownerVideos.length} {spragat(ownerVideos.length, 'видос', 'видоса', 'видосов')})
                    </div>
                    <Row>
                    {ownerVideos.map(
                        video =>
                            <Col key={video.id.toString()} lg={3} md={4}>
                                <Video video={video} />
                            </Col>)}
                    </Row>
                </>;
            }
        );
    })();

    return (
        <Container>
            <div className={style.title}>Ваши подписки</div>
            {content}
        </Container>
    );
}
