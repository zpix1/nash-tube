import style from './SearchScreen.module.scss';
import { Container, Col, Row } from 'react-bootstrap';
import { useRouter } from 'next/router';
import VideoHorizontal from '../../elements/video/VideoHorizontal';
import { fetchJson } from '../../../lib/fetchJson';
import { useEffect, useState } from 'react';
import { RuLoader } from '../homescreen/RuLoader';

const getVideosByQuery = async (query) => {
    return await fetchJson(`/api/videos/search?query=${query}`, 'GET', undefined);
};

export default function SearchScreen(props) {
    const router = useRouter();
    const { search_str: searchString } = router.query;
    const [videos, setVideos] = useState();

    useEffect(() => {
        if (searchString || searchString === '') {
            (async () => {
                setVideos(await getVideosByQuery(searchString));
            })();
        }
    }, [searchString]);

    const content = (() => {
        if (!videos) {
            return <RuLoader />;
        }
        if (videos.length === 0) {
            return (
                <div
                    style={{
                        fontSize: '30px',
                        textAlign: 'center',
                        padding: 50
                    }}
                >
                    Видео не найдены
                </div>
            );
        }
        return videos.map((video, i) => (
            <Row key={i} className={style.searchscreen__videoblock}>
                <VideoHorizontal video={video} />
            </Row>
        ));
    })();

    return (
        <Container>
            {content}
        </Container>
    );
}
