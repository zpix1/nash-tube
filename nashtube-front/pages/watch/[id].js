import MainLayout from '../../components/layouts/MainLayout';
import WatchScreen from '../../components/screens/watchscreen/WatchScreen';
import { useRouter } from 'next/router';

export default function WatchPage() {
    const router = useRouter();
    const { id } = router.query;
    return (
        <>
            <MainLayout>
                <WatchScreen id={id} />
            </MainLayout>
        </>
    );
}