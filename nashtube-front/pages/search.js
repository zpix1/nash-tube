import MainLayout from "../components/layouts/MainLayout";
import SearchScreen from "../components/screens/searchscreen/SearchScreen";
export default function Search() {
  return(
    <>
    <MainLayout>
      <SearchScreen/>
    </MainLayout>
    </>
  )
}