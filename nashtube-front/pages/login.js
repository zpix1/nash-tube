import { useQuery } from 'react-query';
import SignInForm from '../components/screens/authscreen/SignInForm';
import SignUpForm from '../components/screens/authscreen/SignUpForm';
import { useState } from 'react';

export default function Login() {
    const [isSignIn, setForm] = useState(true);
    const switchForm = () => {
        setForm(value => !value);
    };
    return (
        <>
            {isSignIn ? <SignInForm switchForm={switchForm} /> : <SignUpForm switchForm={switchForm} />}
        </>
    );
}