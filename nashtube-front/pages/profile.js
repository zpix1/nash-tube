import { useQuery } from "react-query";
import MainLayout from "../components/layouts/MainLayout";
import ProfileScreen from "../components/screens/profilescreen/ProfileScreen";
export default function Profile() {
  return(
    <>
    <MainLayout>
      <ProfileScreen/>
    </MainLayout>
    </>
  )
}