import { useQuery } from 'react-query';
import MainLayout from '../components/layouts/MainLayout';
import HomeScreen from '../components/screens/homescreen/HomeScreen';
import { useEffect } from 'react';

export default function Home() {
    return (
        <>
            <MainLayout>
                <HomeScreen />
            </MainLayout>
        </>
    );
}
