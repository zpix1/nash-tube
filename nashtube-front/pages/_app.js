import 'bootstrap/dist/css/bootstrap.min.css'
import "../styles/base/_base.scss"
import Head from 'next/head';
import {
  useQuery,
  useMutation,
  useQueryClient,
  QueryClient,
  QueryClientProvider,
} from "react-query";

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  return (
    <><Head>
          <title>NashTube</title>
    </Head>
    <QueryClientProvider client={queryClient}>
      <Component {...pageProps} />
    </QueryClientProvider></>
    
  );
}

export default MyApp;
