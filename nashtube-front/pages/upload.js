import { useQuery } from "react-query";
import MainLayout from "../components/layouts/MainLayout";
import UploadForm from "../components/screens/uploadscreen/UploadForm";
export default function Upload() {
  return(
    <>
    <MainLayout>
    <UploadForm/>
    </MainLayout>
    </>
  )
  /* const { isLoading, error, data } = useQuery("test", async () => {
    const result = await fetch("/api/test");
    return await result.text();
  });

  if (isLoading) {
    return "Loading...";
  }

  if (error) {
    return "An error has occurred: " + error.message;
  }

  return (
    <>
      <div classNameNameName="container">
        <h1>Nash Tube v1.0</h1>
        Result from api: {data}
      </div>
      <style jsx>{`
      .container {
        padding: 2rem;
        margin: 0 auto;
        max-width: 600px;
      }      
      `}</style>
    </>
  ); */
}