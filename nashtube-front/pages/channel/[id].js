import MainLayout from '../../components/layouts/MainLayout';
import ChannelScreen from '../../components/screens/channelscreen/ChannelScreen';
import { useRouter } from 'next/router';

export default function ChannelPage() {
    const router = useRouter();
    const { id } = router.query;
    return (
        <>
            <MainLayout>
                <ChannelScreen id={id} />
            </MainLayout>
        </>
    );
}