import { useQuery } from 'react-query';
import MainLayout from '../components/layouts/MainLayout';
import HomeScreen from '../components/screens/homescreen/HomeScreen';
import { useEffect } from 'react';
import SubScreen from '../components/screens/subscreen/SubScreen';

export default function Subs() {
    return (
        <>
            <MainLayout>
                <SubScreen />
            </MainLayout>
        </>
    );
}
