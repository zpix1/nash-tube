package com.nashtube;

import com.nashtube.auth.AuthenticationServiceImpl;
import com.nashtube.auth.UserLoginPasswordDAO;
import com.nashtube.subscription.SubscriptionController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SubscriptionControllerTests {

    @Autowired
    private SubscriptionController sContr;
    @Autowired
    private AuthenticationServiceImpl aServ;

    @Test
    void subscribeReturns401StatusWhenNotAuthorized() {
        Assertions.assertEquals(401, sContr.subscribe(1L).getStatusCode().value(),
                "Should return 401 when unauthorized");
    }

    @Test
    void getAllSubscribeesReturns401StatusWhenNotAuthorized() {
        Assertions.assertEquals(401, sContr.getAllSubscribees().getStatusCode().value(),
                "Should return 401 when unauthorized");
    }

    @Test
    void getAllSubscribeesVideosReturns401StatusWhenNotAuthorized() {
        Assertions.assertEquals(401, sContr.getAllSubscribeesVideos().getStatusCode().value(),
                "Should return 401 when unauthorized");
    }

    @Test
    void getAllSubscribeesVideosReturns200StatusWhenAuthorized() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        Assertions.assertEquals(200, sContr.getAllSubscribeesVideos().getStatusCode().value(),
                "Should return 200 when authorized");
    }

    @Test
    void subscribeReturns200StatusWhenAuthorized() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        aServ.register(new UserLoginPasswordDAO("username1", "password1"));
        Assertions.assertEquals(200, sContr.subscribe(2L).getStatusCode().value(),
                "Should return 200 when authorized");
    }

    @Test
    void getAllSubscribeesReturns200StatusWhenAuthorized() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        aServ.register(new UserLoginPasswordDAO("username1", "password1"));
        Assertions.assertEquals(200, sContr.getAllSubscribees().getStatusCode().value(),
                "Should return 200 when authorized");
    }

}
