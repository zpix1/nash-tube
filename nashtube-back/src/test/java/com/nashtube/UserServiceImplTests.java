package com.nashtube;

import com.nashtube.user.NashUser;
import com.nashtube.user.UserRepository;
import com.nashtube.user.UserServiceImpl;
import com.nashtube.video.Video;
import com.nashtube.video.VideoRepository;
import com.nashtube.video.views.VideoView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class UserServiceImplTests {
    @Autowired
    private UserRepository uRep;
    @Autowired
    private UserServiceImpl uServ;
    @Autowired
    VideoRepository vRep;

    @Test
    void existentUserLoaded() {
        NashUser user = new NashUser(1L, "username", "password", 0L, null);
        uRep.save(user);
        UserDetails details = uServ.loadUserByUsername(user.getUsername());
        Assertions.assertNotNull(details, "Registered user should be loaded correctly. It should not be null");
        Assertions.assertEquals(details.getUsername(), user.getUsername(), "Username of loaded user should ne equal to username we give to function as an argument");
    }

    @Test
    void throwsWhenUserDoesNotExist() {
        Assertions.assertThrows(UsernameNotFoundException.class, () -> uServ.loadUserByUsername("username"), "exception is excepted if there is no such user registered");
    }

    @Test
    void existentUserFound() {
        NashUser user = new NashUser(1L, "username", "password", 0L, null);
        uRep.save(user);
        NashUser found = uServ.getUserById(1L);
        Assertions.assertNotNull(found, "Registered user should be loaded correctly. It should not be null");
        Assertions.assertEquals(found.getUsername(), user.getUsername(), "Username of loaded user should ne equal to username we give to function as an argument");
    }

    @Test
    void nullWhenUserDoesNotExist() {
        Assertions.assertNull(uServ.getUserById(1L), "should be null if there is no such user registered");
    }

    @Test
    void findByIdForChannelSuccess() {
        NashUser user = new NashUser(1L, "username", "password", 0L, null);
        uRep.save(user);
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint", 100L, new NashUser(1L, null, null, 0L, null), 0L, 0L, 0L));
        Assertions.assertNotNull(uServ.findByIdForChannel(1L), "Достали пользователя из базы");
    }
}