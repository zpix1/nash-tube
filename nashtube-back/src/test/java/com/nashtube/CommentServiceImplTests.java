package com.nashtube;

import com.nashtube.auth.AuthenticationServiceImpl;
import com.nashtube.auth.UserLoginPasswordDAO;
import com.nashtube.comment.CommentServiceImpl;
import com.nashtube.comment.dao.CommentRequestDao;
import com.nashtube.user.NashUser;
import com.nashtube.video.Video;
import com.nashtube.video.VideoRepository;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.sql.SQLException;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CommentServiceImplTests {

    @Autowired
    private CommentServiceImpl cServ;

    @Autowired
    private AuthenticationServiceImpl aServ;
    @Autowired
    private VideoRepository vRep;

    @Test
    void throwsSqlExceptionWhenNoCommentWithSuchId() {
        Assertions.assertThrows(SQLException.class,
                () -> cServ.deleteCommentById(1L, 1L),
                "Should throw when no comment with such id");
    }

    @Test
    void throwsAuthorizationExceptionWhenCommentOwnerIsDifferent() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        aServ.register(new UserLoginPasswordDAO("username1", "password1"));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                100L,
                new NashUser(1L, null, null, 0L, null),
                0L, 0L, 0L));
        cServ.addComment(new CommentRequestDao(2L, 1L, "Hi"));
        Assertions.assertThrows(AuthorizationException.class,
                () -> cServ.deleteCommentById(1L, 1L),
                "Should throw when no comment with such id");
    }

}
