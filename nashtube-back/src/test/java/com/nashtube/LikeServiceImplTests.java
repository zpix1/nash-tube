package com.nashtube;

import com.nashtube.like.LikeEnum;
import com.nashtube.like.LikeRepository;
import com.nashtube.like.LikeServiceImpl;
import com.nashtube.user.NashUser;
import com.nashtube.user.UserRepository;
import com.nashtube.video.Video;
import com.nashtube.video.VideoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class LikeServiceImplTests {

    @Autowired
    private LikeServiceImpl lServ;
    @Autowired
    private UserRepository uRep;
    @Autowired
    private VideoRepository vRep;
    @Autowired
    private LikeRepository lRep;

    @Test
    void wasNotRatedBeforeScenario() {
        uRep.save(new NashUser(null, "username", "password", 0L, null));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                1000L,
                new NashUser(1L, null, null, null, null),
                0L, 0L, 0L));
        lServ.rate(1L, 1L, LikeEnum.LIKE);
        Assertions.assertTrue(lRep.findById(1L).isPresent(),
                "New like should present");
        Assertions.assertEquals(1L, vRep.findById(1L).get().getLikes(),
                "One like should be added to video");
    }

    @Test
    void wasRatedTheSameBeforeScenario() {
        uRep.save(new NashUser(null, "username", "password", 0L, null));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                1000L,
                new NashUser(1L, null, null, null, null),
                0L, 0L, 0L));
        lServ.rate(1L, 1L, LikeEnum.LIKE);
        lServ.rate(1L, 1L, LikeEnum.LIKE);
        Assertions.assertTrue(lRep.findById(1L).isEmpty(),
                "Like should disappear from database");
        Assertions.assertEquals(0L, vRep.findById(1L).get().getLikes(),
                "Like counter should return to zero");
    }

    @Test
    void wasRatedTheOppositeBeforeScenario() {
        uRep.save(new NashUser(null, "username", "password", 0L, null));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                10000L,
                new NashUser(1L, null, null, null, null),
                0L, 0L, 0L));
        lServ.rate(1L, 1L, LikeEnum.LIKE);
        lServ.rate(1L, 1L, LikeEnum.DISLIKE);
        Assertions.assertTrue(lRep.findById(1L).isPresent(),
                "One dislike should present");
        Assertions.assertEquals(LikeEnum.DISLIKE, lRep.findById(1L).get().getType(),
                "Like should change to dislike");
        Assertions.assertEquals(0L, vRep.findById(1L).get().getLikes(),
                "Zero likes should be on video after changing to dislike");
        Assertions.assertEquals(1L, vRep.findById(1L).get().getDislikes(),
                "One dislike should be on video after changing to dislike");
    }

    @Test
    void isLikedByReturnsNullWhenNotLiked() {
        uRep.save(new NashUser(null, "username", "password", 0L, null));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                1000L,
                new NashUser(1L, null, null, null, null),
                0L, 0L, 0L));
        Assertions.assertNull(lServ.isLikedBy(1L, 1L),
                "Should be null because video is not liked");
    }

    @Test
    void isLikedByReturnsCorrectValueWhenLiked() {
        uRep.save(new NashUser(null, "username", "password", 0L, null));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                1000L,
                new NashUser(1L, null, null, null, null),
                0L, 0L, 0L));
        lServ.rate(1L, 1L, LikeEnum.LIKE);
        Assertions.assertEquals(LikeEnum.LIKE, lServ.isLikedBy(1L, 1L),
                "Should be LIKE because video is liked");
    }

}
