package com.nashtube;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nashtube.auth.AuthenticationServiceImpl;
import com.nashtube.user.UserRepository;
import com.nashtube.video.VideoController;
import com.nashtube.video.VideoRepository;
import com.nashtube.video.VideoService;
import com.nashtube.video.views.VideoView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class VideoControllerTests {

    @Autowired
    VideoController vContr;
    @Autowired
    VideoRepository vRep;
    @Autowired
    UserRepository uRep;
    @Autowired
    AuthenticationServiceImpl aServ;
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VideoService service;


    @Test
    void handleFileParseFailedExceptionReturns400Status() {
        Assertions.assertEquals(400, vContr.handleFileParseFailedException().getStatusCode().value(), "Should return 400");
    }

    @Test
    void handleDataIntegrityViolationExceptionReturns400Status() {
        Assertions.assertEquals(400, vContr.handleDataIntegrityViolationException().getStatusCode().value(), "Should return 400");
    }

    @Test
    @WithMockUser
    void addVideoReturns400StatusWhenNotAllRequiredInformationReceived() throws Exception {
        mockMvc.perform(post("/api/channel/1/videos/upload").sessionAttrs(Map.of("userid", 1))).andExpect(status().isBadRequest());
    }

    @Test
    void addVideoReturns401StatusWhenNotAuthorized() throws Exception {
        Map<String, Object> body = Map.of("name", "test");
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(post("/api/channel/1/videos/upload").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(body))).andExpect(status().is(401));
    }

    @WithMockUser
    @Test
    void addVideoSuccess() throws Exception {
        Map<String, Object> body = Map.of("name", "test", "description", "test", "duration", 100L);
        ObjectMapper objectMapper = new ObjectMapper();
        when(service.uploadVideo(1L, "test", "test", 100L)).thenReturn(new VideoService.UploadVideoResult(1L, "test", "test"));
        mockMvc.perform(post("/api/channel/1/videos/upload").sessionAttrs(Map.of("userid", 1L)).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(body))).andExpect(status().is(200));
    }

    @Test
    void deleteVideoNoVId() throws Exception {
        mockMvc.perform(delete("/api/channel/1/videos/delete").sessionAttrs(Map.of("userid", 1L))).andExpect(status().isBadRequest());
    }

    @Test
    void deleteVideoUnauthorized() throws Exception {
        mockMvc.perform(delete("/api/channel/1/videos/delete?vId=1")).andExpect(status().is(401));
    }

    @Test
    @WithMockUser
    void deleteVideoAnotherChannel() throws Exception {
        mockMvc.perform(delete("/api/channel/1/videos/delete?vId=1").sessionAttrs(Map.of("userid", 2L))).andExpect(status().is(403));
    }

    @Test
    @WithMockUser
    void deleteVideoSuccess() throws Exception {
        doNothing().when(service).deleteVideo(1L);
        mockMvc.perform(delete("/api/channel/1/videos/delete?vId=1").sessionAttrs(Map.of("userid", 1L))).andExpect(status().is(200));
    }

    @Test
    void getVideoHeaderWhenVideoNotPresent() throws Exception {
        when(service.getVideo(1L)).thenReturn(null);
        mockMvc.perform(get("/api/videos/get/data?id=1")).andExpect(status().is(400));
    }

    @Test
    void getVideoHeaderSuccess() throws Exception {
        when(service.getVideo(1L)).thenReturn(new VideoView());
        mockMvc.perform(get("/api/videos/get/data?id=1")).andExpect(status().is(200));
    }

    @Test
    void getVideoHeadersTest() throws Exception {
        when(service.getRandomVideosHeaders()).thenReturn(List.of(new VideoView()));
        mockMvc.perform(get("/api/videos/get/random")).andExpect(status().is(200));
    }

    @Test
    void searchVideoSuccess() throws Exception {
        when(service.searchVideos("query")).thenReturn(List.of(new VideoView()));
        mockMvc.perform(get("/api/videos/search?query=query")).andExpect(status().is(200));
    }

    @Test
    void searchWhenThereAreNoVideos() throws Exception {
        when(service.getRandomVideosHeaders()).thenReturn(List.of(new VideoView()));
        mockMvc.perform(get("/api/videos/search?query=\"\"")).andExpect(status().is(200));
    }
}
