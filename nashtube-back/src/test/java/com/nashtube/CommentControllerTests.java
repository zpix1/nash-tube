package com.nashtube;

import com.nashtube.auth.AuthenticationServiceImpl;
import com.nashtube.auth.UserLoginPasswordDAO;
import com.nashtube.comment.CommentController;
import com.nashtube.comment.dao.CommentRequestDao;
import com.nashtube.user.NashUser;
import com.nashtube.video.Video;
import com.nashtube.video.VideoRepository;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.sql.SQLException;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CommentControllerTests {
    @Autowired
    private AuthenticationServiceImpl aServ;
    @Autowired
    private CommentController cContr;
    @Autowired
    private VideoRepository vRep;

    @Test
    void addCommentReturns401StatusWhenNotAuthorized() {
        Assertions.assertEquals(401, cContr.addComment(null).getStatusCode().value(),
                "Should return 401 when unauthorized");
    }

    @Test
    void deleteCommentReturns401StatusWhenNotAuthorized() throws SQLException, AuthorizationException {
        Assertions.assertEquals(401, cContr.deleteComment(1L).getStatusCode().value(),
                "Should return 401 when unauthorized");
    }

    @Test
    void addCommentReturns403StatusWhenForbidden() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        aServ.register(new UserLoginPasswordDAO("username1", "password1"));
        Assertions.assertEquals(403,
                cContr.addComment(new CommentRequestDao(2L, null, null)).getStatusCode().value(),
                "Should return 403 when forbidden");
    }

    @Test
    void addCommentReturns200StatusWhenSuccessful() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                100L,
                new NashUser(1L, null, null, 0L, null),
                0L, 0L, 0L));
        Assertions.assertEquals(200,
                cContr.addComment(new CommentRequestDao(1L, 1L, "Hi")).getStatusCode().value(),
                "Should return 200 when successful");
    }

    @Test
    void deleteCommentReturns200StatusWhenSuccessful() throws SQLException, AuthorizationException {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                1000L,
                new NashUser(1L, null, null, 0L, null),
                0L, 0L, 0L));
        cContr.addComment(new CommentRequestDao(1L, 1L, "Hi"));
        Assertions.assertEquals(200,
                cContr.deleteComment(1L).getStatusCode().value(),
                "Should return 200 when successful");
    }

    @Test
    void getCommentsReturns200StatusWhenSuccessful() throws SQLException, AuthorizationException {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        vRep.save(new Video(1L, null, "kotik", "video about Kotik the Saint",
                1000L,
                new NashUser(1L, null, null, 0L, null),
                0L, 0L, 0L));
        cContr.addComment(new CommentRequestDao(1L, 1L, "Hi"));
        Assertions.assertEquals(200,
                cContr.getComments(1L).getStatusCode().value(),
                "Should return 200 when successful");
    }
}
