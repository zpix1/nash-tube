package com.nashtube;

import com.nashtube.auth.AuthenticationServiceImpl;
import com.nashtube.auth.UserLoginPasswordDAO;
import com.nashtube.subscription.SubscriptionRepository;
import com.nashtube.subscription.SubscriptionServiceImpl;
import com.nashtube.user.NashUser;
import com.nashtube.video.Video;
import com.nashtube.video.VideoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class SubscriptionServiceImplTests {

    @Autowired
    private SubscriptionServiceImpl sServ;
    @Autowired
    private AuthenticationServiceImpl aServ;
    @Autowired
    private SubscriptionRepository sRep;
    @Autowired
    private VideoRepository vRep;

    @Test
    void SubscriptionSuccessfulTest() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        aServ.register(new UserLoginPasswordDAO("username1", "password1"));
        sServ.subscribe(1L, 2L);
        Assertions.assertEquals(1, sRep.findAllBySubscriberId(1L).size(),
                "One subscription should present");
    }

    @Test
    void isSubscribedTrueTest() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        aServ.register(new UserLoginPasswordDAO("username1", "password1"));
        sServ.subscribe(1L, 2L);
        Assertions.assertTrue(sServ.isSubscribedOn(1L, 2L),
                "Should return true because subscribed");
    }

    @Test
    void isSubscribedFalseTest() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        aServ.register(new UserLoginPasswordDAO("username1", "password1"));
        Assertions.assertFalse(sServ.isSubscribedOn(1L, 2L),
                "Should return false because not subscribed");
    }

    @Test
    void UnsubscriptionSuccessfulTest() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        aServ.register(new UserLoginPasswordDAO("username1", "password1"));
        sServ.subscribe(1L, 2L);
        sServ.subscribe(1L, 2L);
        Assertions.assertEquals(0, sRep.findAllBySubscriberId(1L).size(),
                "No subscription should present");
    }

    @Test
    void GetAllSubscribeesSuccessfulTest() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        aServ.register(new UserLoginPasswordDAO("username1", "password1"));
        sServ.subscribe(1L, 2L);
        Assertions.assertEquals(2L, sServ.getAllSubscribees(1L).get(0).getId(),
                "Should find new subscribee");
    }

    @Test
    void GetSubscribeesVideosReturnsCorrectList() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        aServ.register(new UserLoginPasswordDAO("username1", "password1"));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                100L,
                new NashUser(2L, null, null, null, null),
                0L, 0L, 0L));
        sServ.subscribe(1L, 2L);
        Assertions.assertEquals("kotik", sServ.getSubscribeesVideos(1L).get(0).getName(),
                "Should return video of subscribee");
    }

}
