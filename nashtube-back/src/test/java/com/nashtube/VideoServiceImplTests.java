package com.nashtube;

import com.nashtube.user.NashUser;
import com.nashtube.user.UserRepository;
import com.nashtube.video.Video;
import com.nashtube.video.VideoRepository;
import com.nashtube.video.VideoService;
import com.nashtube.video.VideoServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class VideoServiceImplTests {

    @Autowired
    VideoServiceImpl vServ;
    @Autowired
    VideoRepository vRep;
    @Autowired
    UserRepository uRep;
    @Autowired
    private ResourceLoader resourceLoader;

    @Test
    void getVideoSucceedsWhenVideoExists() {
        uRep.save(new NashUser(1L, "username", "password", 0L, null));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint", 1000L, new NashUser(1L, null, null, null, null), 0L, 0L, 0L));
        Assertions.assertNotNull(vServ.getVideo(1L), "Should not be null when video exists");
    }

    @Test
    void getRandomVideoHeadersReturnsAddedVideo() {
        uRep.save(new NashUser(1L, "username", "password", 0L, null));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint", 1000L, new NashUser(1L, null, null, null, null), 0L, 0L, 0L));
        Assertions.assertEquals("kotik", vServ.getRandomVideosHeaders().get(0).getName(), "Should return added video");
    }

    @Test
    void deletesVideoWhenItExists() {
        uRep.save(new NashUser(1L, "username", "password", 0L, null));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint", 1000L, new NashUser(1L, null, null, null, null), 0L, 0L, 0L));
        Assertions.assertDoesNotThrow(() -> vServ.deleteVideo(1L), "Should not throw because video exists");
    }

    @Test
    void addVideoSuccess() {
        uRep.save(new NashUser(1L, "username", "password", 0L, null));
        VideoService.UploadVideoResult res = vServ.uploadVideo(1L, "test", "test", 100L);
        Assertions.assertNotNull(vServ.getVideo(res.getId()), "После загрузки видео есть в базе");
    }
}
