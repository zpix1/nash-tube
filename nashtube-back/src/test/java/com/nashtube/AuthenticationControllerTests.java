package com.nashtube;

import com.nashtube.auth.AuthenticationController;
import com.nashtube.auth.UserLoginPasswordDAO;
import com.nashtube.user.NashUser;
import com.nashtube.user.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class AuthenticationControllerTests {

    @Autowired
    private AuthenticationController controller;
    @Autowired
    private UserRepository uRep;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Test
    void returnsOkOnSuccessfulRegistration() {
        UserLoginPasswordDAO info = new UserLoginPasswordDAO("username", "password");
        ResponseEntity<?> entity = controller.register(info);
        Assertions.assertEquals(HttpStatus.OK, entity.getStatusCode(),
                "Should respond with code 200 after successful registration");
    }

    @Test
    void returnsOkOnSuccessfulLogin() {
        UserLoginPasswordDAO info = new UserLoginPasswordDAO("username", "password");
        uRep.save(new NashUser(null, info.getUsername(), encoder.encode(info.getPassword()), 0L, null));
        ResponseEntity<?> entity = controller.login(info);
        Assertions.assertEquals(HttpStatus.OK, entity.getStatusCode(),
                "Should respond with code 200 after successful login");
    }

    @Test
    void throwsWhenRequiredParameterIsNotSentForRegistration() {
        UserLoginPasswordDAO info = new UserLoginPasswordDAO(null, null);
        Assertions.assertThrows(
                DataIntegrityViolationException.class,
                () -> controller.register(info),
                "Should throw an exception if required data was not sent for registration"
        );
    }

    @Test
    void throwsWhenRequiredParameterIsNotSentForLogin() {
        UserLoginPasswordDAO info = new UserLoginPasswordDAO(null, null);
        Assertions.assertThrows(
                BadCredentialsException.class,
                () -> controller.login(info),
                "Should throw an exception if required data was not sent for login"
        );
    }

    @Test
    void dataIntegrityViolationExceptionHandlerRespondsWithBadRequestStatus() {
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, controller.handleDataIntegrityViolationException().getStatusCode(),
                "Handler should respond with nad request");
    }

    @Test
    void handleBadCredentialsExceptionHandlerRespondsWithBadRequestStatus() {
        Assertions.assertEquals(HttpStatus.UNAUTHORIZED, controller.handleBadCredentialsException().getStatusCode(),
                "Handler should respond with nad request");
    }
}
