package com.nashtube;

import com.nashtube.user.NashUser;
import com.nashtube.user.UserController;
import com.nashtube.user.UserRepository;
import com.nashtube.user.UserServiceImpl;
import com.nashtube.video.Video;
import com.nashtube.video.VideoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {
    @Autowired
    UserController uContr;
    @Autowired
    VideoRepository vRep;
    @Autowired
    UserRepository uRep;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserServiceImpl service;

    @Test
    @WithMockUser(username = "test@example.com")
    void getSelfInfo() throws Exception {
        when(service.getUserByUsername("test@example.com")).thenReturn(new NashUser(1L, "test@example.com", "password", 0L, null));
        mockMvc.perform(get("/api/user/self")).andExpect(status().isOk()).andExpect(content().json("{\"status\": \"ok\", \"user\": {\"username\":  \"test@example.com\", \"id\": 1}}"));
    }

    @Test
    void getSelfInfoWhenNotAuthenticated() throws Exception {
        mockMvc.perform(get("/api/user/self")).andExpect(status().isOk()).andExpect(content().json("{\"status\": \"ok\", \"user\": null}"));
    }

    @Test
    void getUserChannel() {
        uRep.save(new NashUser(1L, "username", "password", 0L, null));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint", 100L, new NashUser(1L, null, null, 0L, null), 0L, 0L, 0L));

        Assertions.assertEquals(200, uContr.getChannel(1L).getStatusCode().value(), "Канал есть в базе: статус 200");
    }

    @Test
    void getUserInfo() throws Exception {
        when(service.getUserById(1L)).thenReturn(new NashUser(1L, "test@example.com", "password", 0L, null));
        mockMvc.perform(get("/api/user/get?id=1")).andExpect(status().isOk());
    }
}
