package com.nashtube;

import com.nashtube.test.TestController;
import com.nashtube.user.NashUser;
import com.nashtube.user.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class TestControllerTestClass {

    @Autowired
    private TestController controller;
    @Autowired
    private UserRepository uRep;

    @Test
    void existentUserFound() {
        uRep.save(new NashUser(null, "username", "password", 0L, null));
        Assertions.assertEquals("username", controller.getUsername("username"), "hehe");
    }

    @Test
    void throwsWhenUserDoesNotExist() {
        Assertions.assertEquals("No such user", controller.getUsername("username"), "hehe");
    }
}
