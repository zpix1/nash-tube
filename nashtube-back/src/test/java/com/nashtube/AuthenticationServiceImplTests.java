package com.nashtube;

import com.nashtube.auth.AuthenticationServiceImpl;
import com.nashtube.auth.UserLoginPasswordDAO;
import com.nashtube.user.NashUser;
import com.nashtube.user.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class AuthenticationServiceImplTests {

    @Autowired
    private AuthenticationServiceImpl authenticationService;
    @Autowired
    private UserRepository uRep;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Test
    void userRegisteredCorrectlyWhenUsernameIsUnique() {
        UserLoginPasswordDAO info = new UserLoginPasswordDAO("username", "password");
        Assertions.assertDoesNotThrow(() -> authenticationService.register(info),
                "User with such username was not registered before, so everything should be fine");
        NashUser found = uRep.findNashUserByUsername(info.getUsername());
        Assertions.assertNotNull(found, "User should be found in database after registration");
        Assertions.assertEquals(found.getUsername(), info.getUsername(), "Correct username should be found");
    }

    @Test
    void throwsWhenUserWithSuchUsernameIsAlreadyRegistered() {
        UserLoginPasswordDAO info = new UserLoginPasswordDAO("username", "password");
        uRep.save(new NashUser(null, info.getUsername(), info.getPassword(), 0L, null));
        Assertions.assertThrows(
                DataIntegrityViolationException.class,
                () -> authenticationService.register(info),
                "Should throw exception when user with such username exists"
        );
    }

    @Test
    void userLoggedInCorrectlyWhenCredentialsAreCorrect() {
        UserLoginPasswordDAO info = new UserLoginPasswordDAO("username", "password");
        uRep.save(new NashUser(null, info.getUsername(), encoder.encode(info.getPassword()), 0L, null));
        Assertions.assertDoesNotThrow(() -> authenticationService.login(info),
                "Credentials are correct, so everything should be fine");
    }

    @Test
    void throwsWhenThereIsNoUserWithSuchUsernameRegistered() {
        UserLoginPasswordDAO info = new UserLoginPasswordDAO("username", "password");
        Assertions.assertThrows(BadCredentialsException.class, () -> authenticationService.login(info),
                "No user with such username registered, so should throw an exception");
    }

    @Test
    void throwsWhenPasswordInIncorrect() {
        UserLoginPasswordDAO info = new UserLoginPasswordDAO("username", "password");
        UserLoginPasswordDAO wrong = new UserLoginPasswordDAO("username", "wrong");
        uRep.save(new NashUser(null, info.getUsername(), encoder.encode(info.getPassword()), 0L, null));
        Assertions.assertThrows(BadCredentialsException.class, () -> authenticationService.login(wrong),
                "Password is incorrect, so should throw an exception");
    }

}
