package com.nashtube;

import com.nashtube.auth.AuthenticationServiceImpl;
import com.nashtube.auth.UserLoginPasswordDAO;
import com.nashtube.like.LikeController;
import com.nashtube.user.NashUser;
import com.nashtube.video.Video;
import com.nashtube.video.VideoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
class LikeControllerTests {

    @Autowired
    private LikeController lContr;
    @Autowired
    private VideoRepository vRep;
    @Autowired
    private AuthenticationServiceImpl aServ;

    @Test
    void doLikeReturns401StatusWhenNotAuthorized() {
        Assertions.assertEquals(401, lContr.doLike(1L).getStatusCode().value(),
                "Should return 401 when unauthorized");
    }

    @Test
    void doDislikeReturns401StatusWhenNotAuthorized() {
        Assertions.assertEquals(401, lContr.doDislike(1L).getStatusCode().value(),
                "Should return 401 when unauthorized");
    }

    @Test
    void doLikeReturns200StatusWhenAuthorized() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                1000L,
                new NashUser(1L, null, null, 0L, null),
                0L, 0L, 0L));
        Assertions.assertEquals(200, lContr.doLike(1L).getStatusCode().value(),
                "Should return 200 when authorized");
    }

    @Test
    void doDislikeReturns200StatusWhenAuthorized() {
        aServ.register(new UserLoginPasswordDAO("username", "password"));
        aServ.login(new UserLoginPasswordDAO("username", "password"));
        vRep.save(new Video(null, null, "kotik", "video about Kotik the Saint",
                1000L,
                new NashUser(1L, null, null, 0L, null),
                0L, 0L, 0L));
        Assertions.assertEquals(200, lContr.doDislike(1L).getStatusCode().value(),
                "Should return 200 when authorized");
    }

}



