package com.nashtube.helpers;

import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.spi.MetadataBuilderContributor;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.BooleanType;
import org.hibernate.type.FloatType;

public class SqlFunctionsMetadataBuilderContributor implements MetadataBuilderContributor {
    public static final String function = "to_tsvector(name || ' ' || description || ' ' || "
            + "regexp_replace(username, '@.+', '', 'g'))"
            + "@@ plainto_tsquery(?1)";

    @Override
    public void contribute(MetadataBuilder metadataBuilder) {
        // TODO:
        // Это можно сильно оптимизировать: хранить to_tsvector(...) в отдельном поле и сделать по нему индекс
        metadataBuilder
                .applySqlFunction(
                        "fts",
                        new SQLFunctionTemplate(
                                BooleanType.INSTANCE,
                                function
                        ))
                .applySqlFunction(
                        "ftsc",
                        new SQLFunctionTemplate(
                                FloatType.INSTANCE,
                                function
                        ));
    }
}
