package com.nashtube.test;

import com.nashtube.user.NashUser;
import com.nashtube.user.UserRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api")
public class TestController {

    private final UserRepository uRep;

    public TestController(UserRepository uRep) {
        this.uRep = uRep;
    }

    @GetMapping(path = "test")
    public String getUsername(@RequestParam String username) {
        NashUser user = uRep.findNashUserByUsername(username);
        if (user == null)
            return "No such user";
        return user.getUsername();
    }

}
