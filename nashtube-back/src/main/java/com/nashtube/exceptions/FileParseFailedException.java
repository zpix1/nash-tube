package com.nashtube.exceptions;

public class FileParseFailedException extends RuntimeException {
    public FileParseFailedException(String message) {
        super(message);
    }
}
