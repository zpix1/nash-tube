package com.nashtube.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Blob;

@Repository
public interface UserRepository extends JpaRepository<NashUser, Long> {
    NashUser findNashUserByUsername(String username);

    @Query("update NashUser u set u.subscribers = u.subscribers + 1 where u.id = ?1")
    @Modifying
    void incrementSubscribersOf(Long uId);

    @Query("update NashUser u set u.subscribers = u.subscribers - 1 where u.id = ?1")
    @Modifying
    void decrementSubscribersOf(Long uId);

    @Query("update NashUser u set u.avatar = ?2 where u.id = ?1")
    @Modifying
    void changeUserAvatar(Long uId, Blob avatar);

    @Query("SELECT NEW com.nashtube.user.UserChannelView(nu.id, nu.username) FROM NashUser nu WHERE nu.id = :id")
    UserChannelView findByIdForChannel(@Param("id") Long id);
}
