package com.nashtube.user;

public interface UserService {
    NashUser getUserById(Long uId);

    NashUser getUserByUsername(String username);
}
