package com.nashtube.user;

import com.nashtube.storage.StorageService;
import com.nashtube.video.VideoRepository;
import com.nashtube.video.VideoService;
import com.nashtube.video.views.VideoView;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.engine.jdbc.BlobProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository uRep;
    private final VideoRepository vRep;
    private final StorageService storageService;
    private final VideoService videoService;

    @Autowired
    public UserServiceImpl(
            UserRepository uRep,
            VideoRepository vRep,
            StorageService storageService,
            VideoService videoService
    ) {
        this.uRep = uRep;
        this.vRep = vRep;
        this.storageService = storageService;
        this.videoService = videoService;
    }

    public NashUser getUserByUsername(String username) {
        return uRep.findNashUserByUsername(username);
    }

    public NashUser getUserById(Long uId) {
        Optional<NashUser> user = uRep.findById(uId);
        if (user.isEmpty())
            return null;
        return user.get();
    }

    public UserChannelView findByIdForChannel(Long id) {
        UserChannelView userChannelView = uRep.findByIdForChannel(id);
        if (userChannelView == null)
            return null;

        List<VideoView> videos = vRep.getVideosForUserChannel(id);
        for (VideoView videoView : videos) {
            String imageUrl = storageService.genLinkForDownload(videoService.genPreviewUrl(videoView.getId()));
            String videoUrl = storageService.genLinkForDownload(videoService.genVideoUrl(videoView.getId()));
            videoView.setImageUrl(imageUrl);
            videoView.setVideoUrl(videoUrl);
        }
        userChannelView.setVideos(videos);
        return userChannelView;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        NashUser user = uRep.findNashUserByUsername(username);
        if (user == null)
            throw new UsernameNotFoundException("No user with such username");
        return User.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .authorities(new ArrayList<>())
                .build();
    }

    @Transactional
    public byte[] getUserAvatar(Long uId) throws SQLException {
        Optional<NashUser> user = uRep.findById(uId);
        if (user.isEmpty())
            throw new DataIntegrityViolationException("Possible constraint violation");
        else if (user.get().getAvatar() == null)
            return null;
        else {
            return user.get().getAvatar().getBytes(1, (int) user.get().getAvatar().length());
        }
    }

    @Transactional
    public void changeUserAvatar(Long uId, MultipartFile file) throws IOException {
        Blob blob = BlobProxy.generateProxy(file.getBytes());
        uRep.changeUserAvatar(uId, blob);
    }

}
