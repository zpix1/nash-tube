package com.nashtube.user;

import com.nashtube.subscription.SubscriptionServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(path = "/api")
@RequiredArgsConstructor
public class UserController {
    private final UserServiceImpl uServ;
    private final SubscriptionServiceImpl sServ;
    @Resource
    private HttpServletRequest request;

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<?> handleDataIntegrityViolationException() {
        return ResponseEntity.status(400).body("Reason: possible constraint violation");
    }

    @GetMapping(path = "/user/channel")
    public ResponseEntity<?> getChannel(@RequestParam(name = "id") Long id) {
        UserChannelView userChannelView = uServ.findByIdForChannel(id);
        return ResponseEntity.ok().body(userChannelView);
    }

    @GetMapping(path = "/user/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUserInfo(@RequestParam(name = "id") Long uId) {
        NashUser user = uServ.getUserById(uId);
        if (user == null)
            return ResponseEntity.badRequest().body("Possible constraint violation");
        return ResponseEntity.ok().body(new UserSecureDao(
                user.getId(),
                user.getUsername(),
                user.getSubscribers(),
                sServ.isSubscribedOn((Long) request.getSession().getAttribute("userid"), uId)));
    }

    @GetMapping(path = "/user/self")
    public ResponseEntity<?> getSelfInfo(Principal principal) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("status", "ok");
        if (principal == null) {
            resp.put("user", null);
            return ResponseEntity.ok().body(resp);
        }
        String email = principal.getName();
        NashUser user = uServ.getUserByUsername(email);
        resp.put("user", Map.of(
                "id", user.getId(),
                "username", user.getUsername(),
                "subscribers", user.getSubscribers()));
        return ResponseEntity.ok().body(resp);
    }

    @GetMapping(value = "/user/avatar", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<?> getUserAvatar(@RequestParam(name = "id") Long uId) throws SQLException {
        byte[] bytes = uServ.getUserAvatar(uId);
        if (bytes == null)
            return ResponseEntity.noContent().build();
        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(bytes);
    }

    @PostMapping(value = "/user/change_avatar", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> changeUserAvatar(@RequestParam(name = "file") MultipartFile file) throws IOException {
        Object uIdRaw = request.getSession().getAttribute("userid");
        if (uIdRaw == null)
            return ResponseEntity.status(401).build();
        uServ.changeUserAvatar((Long) uIdRaw, file);
        return ResponseEntity.ok().build();
    }
}
