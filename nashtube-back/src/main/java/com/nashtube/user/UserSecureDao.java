package com.nashtube.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;

@Data
@AllArgsConstructor
@Generated
public class UserSecureDao {
    private Long id;
    private String name;
    private Long subscribers;
    private Boolean subscribedAt;
}
