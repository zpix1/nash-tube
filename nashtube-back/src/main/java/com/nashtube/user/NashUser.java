package com.nashtube.user;


import lombok.*;

import javax.persistence.*;
import java.sql.Blob;


@Entity
@Table(name = "nash_user", uniqueConstraints = {
        @UniqueConstraint(name = "UserUsernameUniqueConstraint", columnNames = {"username"})
})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Generated
public class NashUser {

    @Id
    @SequenceGenerator(
            name = "nashUserSequence",
            sequenceName = "nash_user_sequence",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nashUserSequence")
    private Long id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "subscribers", nullable = false)
    private Long subscribers;

    @Lob
    @Column(name = "nash_image")
    private Blob avatar;


}
