package com.nashtube.user;

import com.nashtube.video.views.VideoView;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Generated
public class UserChannelView {
    private Long id;
    private String username;
    private List<VideoView> videos;

    public UserChannelView(Long id, String username) {
        this.id = id;
        this.username = username;
    }
}

