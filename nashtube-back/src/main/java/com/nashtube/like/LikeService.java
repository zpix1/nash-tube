package com.nashtube.like;

public interface LikeService {
    void rate(Long vId, Long uId, LikeEnum rating);

    LikeEnum isLikedBy(Long vId, Long uId);
}
