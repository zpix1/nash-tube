package com.nashtube.like;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public
interface LikeRepository extends JpaRepository<Like, Long> {
    Optional<Like> findByFromIdAndToId(Long from, Long to);
}
