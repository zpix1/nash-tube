package com.nashtube.like;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Generated
public enum LikeEnum {
    LIKE, DISLIKE

}
