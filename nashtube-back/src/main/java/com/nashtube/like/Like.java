package com.nashtube.like;

import com.nashtube.user.NashUser;
import com.nashtube.video.Video;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "like_entity", uniqueConstraints = {
        @UniqueConstraint(name = "LikeVideoUserUniqueConstraint",
                columnNames = {"like_from", "like_to"})
})
@Getter
@Setter
@ToString
@AllArgsConstructor
@Generated
@NoArgsConstructor
public class Like {
    @Id
    @SequenceGenerator(name = "LikeSequence", sequenceName = "like_sequence",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LikeSequence")
    private Long id;
    @ManyToOne(targetEntity = NashUser.class)
    @JoinColumn(name = "like_from", referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "LikeFromForeignKeyConstraint"))
    private NashUser from;
    @ManyToOne(targetEntity = Video.class)
    @JoinColumn(name = "like_to", referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "LikeToForeignKeyConstraint"))
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Video to;
    @Enumerated(EnumType.STRING)
    private LikeEnum type;

}
