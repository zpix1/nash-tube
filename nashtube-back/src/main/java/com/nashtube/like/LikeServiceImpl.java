package com.nashtube.like;

import com.nashtube.user.NashUser;
import com.nashtube.video.Video;
import com.nashtube.video.VideoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LikeServiceImpl implements LikeService {

    private final LikeRepository lRep;
    private final VideoRepository vRep;

    @Transactional
    public void rate(Long vId, Long uId, LikeEnum rating) {
        Optional<Like> oldLike = lRep.findByFromIdAndToId(uId, vId);
        if (oldLike.isEmpty()) {
            Like like = new Like(
                    null,
                    new NashUser(uId,
                            null,
                            null,
                            null,
                            null),
                    new Video(
                            vId,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null
                    ),
                    rating);
            lRep.save(like);
            if (rating == LikeEnum.LIKE)
                vRep.incrementLikesOf(vId);
            else vRep.incrementDislikesOf(vId);
        } else if (oldLike.get().getType() != rating) {
            oldLike.get().setType(rating);
            lRep.save(oldLike.get());
            if (rating == LikeEnum.LIKE) {
                vRep.incrementLikesOf(vId);
                vRep.decrementDislikesOf(vId);
            } else {
                vRep.incrementDislikesOf(vId);
                vRep.decrementLikesOf(vId);
            }
        } else {
            lRep.delete(oldLike.get());
            if (rating == LikeEnum.LIKE)
                vRep.decrementLikesOf(vId);
            else vRep.decrementDislikesOf(vId);
        }
    }

    @Override
    public LikeEnum isLikedBy(Long vId, Long uId) {
        if (uId == null)
            return null;
        Optional<Like> like = lRep.findByFromIdAndToId(uId, vId);
        if (like.isEmpty())
            return null;
        return like.get().getType();
    }

}
