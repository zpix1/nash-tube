package com.nashtube.like;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "api")
@RequiredArgsConstructor
public class LikeController {
    private final LikeServiceImpl lServ;
    @Resource
    private HttpServletRequest request;

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<?> handleDataIntegrityViolationException() {
        return ResponseEntity.status(400).body("Reason: possible constraint violation");
    }

    @PostMapping(path = "/videos/like")
    public ResponseEntity<?> doLike(@RequestParam(name = "id") Long vId) {
        Object uIdRaw = request.getSession().getAttribute("userid");
        if (uIdRaw == null)
            return ResponseEntity.status(401).build();
        lServ.rate(vId, (Long) uIdRaw, LikeEnum.LIKE);
        return ResponseEntity.ok().build();
    }

    @PostMapping(path = "/videos/dislike")
    public ResponseEntity<?> doDislike(@RequestParam(name = "id") Long vId) {
        Object uIdRaw = request.getSession().getAttribute("userid");
        if (uIdRaw == null)
            return ResponseEntity.status(401).build();

        lServ.rate(vId, (Long) uIdRaw, LikeEnum.DISLIKE);
        return ResponseEntity.ok().build();
    }

}
