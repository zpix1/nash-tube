package com.nashtube.comment;

import com.nashtube.comment.dao.CommentRequestDao;
import com.nashtube.user.NashUser;
import com.nashtube.video.Video;
import lombok.RequiredArgsConstructor;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository cRep;

    public List<Comment> getCommentsByVideoId(Long vId) {
        return cRep.findAllByCommentOnId(vId);
    }

    public void deleteCommentById(Long uId, Long cId) throws SQLException, AuthorizationException {
        Optional<Comment> found = cRep.findById(cId);
        if (found.isEmpty())
            throw new SQLException("No comment with such id");
        if (!Objects.equals(found.get().getCommentFrom().getId(), uId))
            throw new AuthorizationException("Not authorized");
        cRep.deleteById(cId);
    }

    public void addComment(CommentRequestDao info) {
        cRep.save(
                new Comment(
                        null,
                        new NashUser(info.getUId(),
                                null,
                                null,
                                null,
                                null),
                        new Video(
                                info.getVId(),
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null
                        ),
                        info.getText(),
                        LocalDateTime.now()));
    }

}
