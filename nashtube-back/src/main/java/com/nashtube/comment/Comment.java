package com.nashtube.comment;

import com.nashtube.user.NashUser;
import com.nashtube.video.Video;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Generated
public class Comment {

    @Id
    @SequenceGenerator(name = "CommentSequence", sequenceName = "comment_sequence",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CommentSequence")
    private Long id;
    @ManyToOne(targetEntity = NashUser.class)
    @JoinColumn(referencedColumnName = "id", nullable = false,
            foreignKey = @ForeignKey(name = "userCommentForeignKey"))
    private NashUser commentFrom;
    @ManyToOne(targetEntity = Video.class)
    @JoinColumn(referencedColumnName = "id", nullable = false,
            foreignKey = @ForeignKey(name = "videoCommentForeignKey"))
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Video commentOn;
    @Column(nullable = false)
    private String commentText;
    @Column(nullable = false)
    private LocalDateTime commentDate;

}
