package com.nashtube.comment;

import com.nashtube.comment.dao.CommentRequestDao;
import com.nashtube.comment.dao.CommentResponseDao;
import lombok.RequiredArgsConstructor;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

@Controller
@RequiredArgsConstructor
@RequestMapping(path = "api/comment")
public class CommentController {

    private final CommentServiceImpl cServ;
    @Resource
    private HttpServletRequest request;

    @ExceptionHandler({SQLException.class, DataIntegrityViolationException.class})
    public ResponseEntity<?> handleSqlException() {
        return ResponseEntity.status(400).body("Reason: possible constraint violation");
    }

    @ExceptionHandler(AuthorizationException.class)
    public ResponseEntity<?> handleAuthorizationException() {
        return ResponseEntity.status(403).build();
    }

    @GetMapping(path = "get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getComments(@RequestParam(name = "id") Long vId) {
        return ResponseEntity.ok().body(cServ.getCommentsByVideoId(vId)
                .stream().map(comment -> new CommentResponseDao(
                        comment.getId(), comment.getCommentFrom().getId(),
                        comment.getCommentOn().getId(),
                        comment.getCommentFrom().getUsername(),
                        comment.getCommentText(), comment.getCommentDate()
                )));
    }

    @PostMapping(path = "add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addComment(@RequestBody CommentRequestDao info) {
        Object uIdRaw = request.getSession().getAttribute("userid");
        if (uIdRaw == null)
            return ResponseEntity.status(401).build();
        Long uId = (Long) uIdRaw;
        if (!uId.equals(info.getUId()))
            return ResponseEntity.status(403).build();
        cServ.addComment(info);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "delete")
    public ResponseEntity<?> deleteComment(@RequestParam(name = "id") Long cId)
            throws SQLException, AuthorizationException {
        Object uIdRaw = request.getSession().getAttribute("userid");
        if (uIdRaw == null)
            return ResponseEntity.status(401).build();
        cServ.deleteCommentById((Long) uIdRaw, cId);
        return ResponseEntity.ok().build();
    }

}
