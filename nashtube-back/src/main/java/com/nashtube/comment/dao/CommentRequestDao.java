package com.nashtube.comment.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;

@Data
@AllArgsConstructor
@Generated
public class CommentRequestDao {
    private Long uId;
    private Long vId;
    private String text;
}
