package com.nashtube.comment.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Generated
public class CommentResponseDao {
    private Long id;
    private Long uId;
    private Long vId;
    private String channelName;
    private String text;
    private LocalDateTime date;
}
