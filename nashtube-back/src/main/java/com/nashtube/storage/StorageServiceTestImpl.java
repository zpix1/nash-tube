package com.nashtube.storage;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("test")
public class StorageServiceTestImpl implements StorageService {
    public String genLinkForUpload(String fileName) {
        return "upload";
    }

    public String genLinkForDownload(String fileName) {
        return "download";
    }

    public void delete(String fileName) {
        // Do nothing
    }
}
