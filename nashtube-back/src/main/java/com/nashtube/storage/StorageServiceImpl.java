package com.nashtube.storage;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.amazonaws.services.xray.model.Http;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.net.URL;

@Service
@Profile("!test")
public class StorageServiceImpl implements StorageService {
    public static final long expInterval = 1000 * 60 * 60L;
    private final AmazonS3 s3Client;
    @Value("${cloud.aws.bucket}")
    private String bucketName;

    public StorageServiceImpl(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }

    public String genLinkForUpload(String fileName) {
        return genLink(fileName, HttpMethod.PUT);
    }

    public String genLinkForDownload(String fileName) {
        return genLink(fileName, HttpMethod.GET);
    }

    public void delete(String fileName) {
        s3Client.deleteObject(new DeleteObjectRequest(bucketName, fileName));
    }

    private String genLink(String fileName, HttpMethod method) {
        java.util.Date expiration = new java.util.Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += expInterval;
        expiration.setTime(expTimeMillis);

        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(
                bucketName,
                fileName
        ).withMethod(method)
                .withExpiration(expiration)
                .withResponseHeaders(new ResponseHeaderOverrides().withCacheControl(
                        "max-age=7776000,must-revalidate, no-transform, public"
                ));
        URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
        return url.toString();
    }
}
