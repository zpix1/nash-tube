package com.nashtube.storage;


public interface StorageService {
    String genLinkForUpload(String fileName);

    String genLinkForDownload(String fileName);

    void delete(String filename);
}
