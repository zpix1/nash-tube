package com.nashtube.subscription;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    Optional<Subscription> findBySubscriberIdAndSubscribeeId(Long uId, Long cId);

    List<Subscription> findAllBySubscriberId(Long uId);

}
