package com.nashtube.subscription;

import com.nashtube.user.UserSecureDao;

import java.util.List;

public interface SubscriptionService {
    boolean isSubscribedOn(Long uId, Long cId);

    List<UserSecureDao> getAllSubscribees(Long uId);

    void subscribe(Long uId, Long cId);
}
