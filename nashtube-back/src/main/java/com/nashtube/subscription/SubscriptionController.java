package com.nashtube.subscription;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "api")
@RequiredArgsConstructor
public class SubscriptionController {

    private static final String USER_ID_KEY = "userid";

    private final SubscriptionServiceImpl sServ;
    @Resource
    private HttpServletRequest request;

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<?> handleDataIntegrityViolationException() {
        return ResponseEntity.status(400).body("Reason: possible constraint violation");
    }

    @PostMapping(path = "channels/subscribe/subscribe")
    public ResponseEntity<?> subscribe(@RequestParam(name = "id") Long cId) {
        Object uIdRaw = request.getSession().getAttribute(USER_ID_KEY);
        if (uIdRaw == null)
            return ResponseEntity.status(401).build();
        sServ.subscribe((Long) uIdRaw, cId);
        return ResponseEntity.ok().build();
    }

    @PostMapping(path = "channels/subscribe/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllSubscribees() {
        Object uIdRaw = request.getSession().getAttribute(USER_ID_KEY);
        if (uIdRaw == null)
            return ResponseEntity.status(401).build();
        return ResponseEntity.ok().body(sServ.getAllSubscribees((Long) uIdRaw));
    }

    @GetMapping(path = "channels/subscribe/videos", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllSubscribeesVideos() {
        Object uIdRaw = request.getSession().getAttribute(USER_ID_KEY);
        if (uIdRaw == null)
            return ResponseEntity.status(401).build();
        return ResponseEntity.ok().body(sServ.getSubscribeesVideos((Long) uIdRaw));
    }

}
