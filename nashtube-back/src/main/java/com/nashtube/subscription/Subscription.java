package com.nashtube.subscription;

import com.nashtube.user.NashUser;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "subscribtion", uniqueConstraints = {
        @UniqueConstraint(name = "SubscriberSubscribeeUniqueConstraint",
                columnNames = {"subscriber", "subscribee"})})
@Getter
@Setter
@ToString
@AllArgsConstructor
@Generated
@NoArgsConstructor
public class Subscription {
    @Id
    @SequenceGenerator(name = "SubscribtionSequence", sequenceName = "subscribtion_sequence",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SubscribtionSequence")
    private Long id;
    @ManyToOne(targetEntity = NashUser.class)
    @JoinColumn(name = "subscriber", referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "subscriberForeignKeyConstraint"))
    private NashUser subscriber;
    @ManyToOne(targetEntity = NashUser.class)
    @JoinColumn(name = "subscribee", referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "subscribeeForeignKeyConstraint"))
    private NashUser subscribee;
}
