package com.nashtube.subscription;

import com.nashtube.like.LikeEnum;
import com.nashtube.like.LikeService;
import com.nashtube.like.LikeServiceImpl;
import com.nashtube.storage.StorageService;
import com.nashtube.user.NashUser;
import com.nashtube.user.UserRepository;
import com.nashtube.user.UserSecureDao;
import com.nashtube.video.Video;
import com.nashtube.video.VideoRepository;
import com.nashtube.video.VideoService;
import com.nashtube.video.views.VideoView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    private final UserRepository uRep;
    private final SubscriptionRepository sRep;

    private final VideoRepository vRep;
    private final LikeService lServ;

    private final StorageService storageService;
    private final VideoService videoService;

    @Resource
    private HttpServletRequest request;

    @Autowired
    public SubscriptionServiceImpl(
            UserRepository uRep,
            SubscriptionRepository sRep,
            VideoRepository vRep,
            LikeService lServ,
            StorageService storageService,
            VideoService videoService,
            HttpServletRequest request
    ) {
        this.uRep = uRep;
        this.sRep = sRep;
        this.vRep = vRep;
        this.lServ = lServ;
        this.storageService = storageService;
        this.videoService = videoService;
        this.request = request;
    }

    @Transactional
    public void subscribe(Long uId, Long cId) {
        Optional<Subscription> found = sRep.findBySubscriberIdAndSubscribeeId(uId, cId);
        if (found.isPresent()) {
            sRep.delete(found.get());
            uRep.decrementSubscribersOf(cId);
        } else {
            sRep.save(new Subscription(
                    null,
                    new NashUser(
                            uId,
                            null,
                            null,
                            null,
                            null
                    ),
                    new NashUser(
                            cId,
                            null,
                            null,
                            null,
                            null
                    )));
            uRep.incrementSubscribersOf(cId);
        }
    }

    public List<UserSecureDao> getAllSubscribees(Long uId) {
        List<Subscription> subscriptions = sRep.findAllBySubscriberId(uId);
        return subscriptions.stream().map(subscription -> new UserSecureDao(
                subscription.getSubscribee().getId(),
                subscription.getSubscribee().getUsername(),
                subscription.getSubscribee().getSubscribers(),
                true
        )).toList();
    }

    public boolean isSubscribedOn(Long uId, Long cId) {
        if (uId == null)
            return false;
        Optional<Subscription> found = sRep.findBySubscriberIdAndSubscribeeId(uId, cId);
        return found.isPresent();
    }

    public List<VideoView> getSubscribeesVideos(Long uId) {
        List<Long> subscribeesIds = getAllSubscribeesIds(uId);
        List<Video> subscribeesVideos = vRep.getVideosBySetOfOwnersIds(subscribeesIds);
        return subscribeesVideos.stream().map(video -> {
            LikeEnum type = lServ.isLikedBy(video.getId(), uId);
            boolean isSubscribedAt = isSubscribedOn(uId, video.getOwner().getId());
            return new VideoView(
                    video.getId(),
                    video.getName(),
                    video.getDescription(),
                    video.getDuration(),
                    video.getOwner().getId(),
                    video.getOwner().getUsername(),
                    video.getLikes(),
                    video.getDislikes(),
                    video.getComments(),
                    LikeEnum.LIKE == type,
                    LikeEnum.DISLIKE == type,
                    video.getOwner().getSubscribers(),
                    isSubscribedAt,
                    storageService.genLinkForDownload(videoService.genPreviewUrl(video.getId())),
                    storageService.genLinkForDownload(videoService.genVideoUrl(video.getId())));
        }).toList();
    }

    private List<Long> getAllSubscribeesIds(Long uId) {
        List<Subscription> subscriptions = sRep.findAllBySubscriberId(uId);
        return subscriptions.stream().map(subscription -> subscription.getSubscribee().getId()).toList();
    }
}
