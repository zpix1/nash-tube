package com.nashtube.auth;

import lombok.Data;
import lombok.Generated;

@Data
@Generated
public class UserLoginPasswordDAO {
    private final String username;
    private final String password;
}
