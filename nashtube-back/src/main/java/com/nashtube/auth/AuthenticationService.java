package com.nashtube.auth;

public interface AuthenticationService {
    void login(UserLoginPasswordDAO info);

    void register(UserLoginPasswordDAO info);
}
