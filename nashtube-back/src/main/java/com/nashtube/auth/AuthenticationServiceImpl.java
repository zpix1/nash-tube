package com.nashtube.auth;

import com.nashtube.user.NashUser;
import com.nashtube.user.UserRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final BCryptPasswordEncoder encoder;
    private final UserRepository uRep;
    @Resource
    private HttpServletRequest request;

    public AuthenticationServiceImpl(
            AuthenticationManager authenticationManager,
            BCryptPasswordEncoder encoder,
            UserRepository uRep
    ) {
        this.authenticationManager = authenticationManager;
        this.encoder = encoder;
        this.uRep = uRep;
    }

    @Override
    public void login(UserLoginPasswordDAO info) {
        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(info.getUsername(), info.getPassword())
        );
        NashUser user = uRep.findNashUserByUsername(info.getUsername());
        request.getSession().setAttribute("userid", user.getId());
        request.getSession().setAttribute("username", user.getUsername());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    @Override
    public void register(UserLoginPasswordDAO info) {
        uRep.save(new NashUser(null, info.getUsername(), encoder.encode(info.getPassword()), 0L, null));
    }
}
