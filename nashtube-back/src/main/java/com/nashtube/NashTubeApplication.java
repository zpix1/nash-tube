package com.nashtube;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NashTubeApplication {


    public static void main(String[] args) {
        SpringApplication.run(NashTubeApplication.class, args);
    }

}
