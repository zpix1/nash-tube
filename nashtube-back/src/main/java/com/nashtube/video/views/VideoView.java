package com.nashtube.video.views;

import lombok.*;

@Getter
@Setter
@Generated
@AllArgsConstructor
@NoArgsConstructor
public class VideoView {
    private Long id;
    private String name;
    private String description;
    private Long duration;
    private Long owner;
    private String ownerName;
    private Long likes;
    private Long dislikes;
    private Long comments;
    private boolean isLiked;
    private boolean isDisliked;
    private Long subscribers;
    private boolean isSubscribedAt;
    private String imageUrl;
    private String videoUrl;

    public VideoView(Long id, Long duration, String name, Long ownerId, String ownerName) {
        this.id = id;
        this.duration = duration;
        this.name = name;
        this.owner = ownerId;
        this.ownerName = ownerName;
    }

    public VideoView(Long id, String name, Long duration) {
        this.id = id;
        this.name = name;
        this.duration = duration;
    }
}
