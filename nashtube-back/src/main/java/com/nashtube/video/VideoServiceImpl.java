package com.nashtube.video;

import com.nashtube.like.LikeEnum;
import com.nashtube.like.LikeService;
import com.nashtube.storage.StorageService;
import com.nashtube.subscription.SubscriptionRepository;
import com.nashtube.user.NashUser;
import com.nashtube.video.views.VideoView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class VideoServiceImpl implements VideoService {

    private final VideoRepository vRep;
    private final LikeService lServ;
    private final SubscriptionRepository subscriptionRepository;
    private final StorageService storageService;
    @Resource
    private HttpServletRequest request;

    @Autowired
    public VideoServiceImpl(
            VideoRepository vRep,
            LikeService lServ,
            SubscriptionRepository subscriptionRepository,
            StorageService storageService
    ) {
        this.vRep = vRep;
        this.lServ = lServ;
        this.subscriptionRepository = subscriptionRepository;
        this.storageService = storageService;
    }

    public String genPreviewUrl(Long previewId) {
        return "/images/previews/" + previewId;
    }

    public String genVideoUrl(Long videoId) {
        return "/videos/" + videoId;
    }

    public UploadVideoResult uploadVideo(Long channelId, String name, String description, Long duration) {
        Video video = new Video(
                null,
                null,
                name,
                description,
                duration,
                new NashUser(channelId, null, null, null, null),
                0L,
                0L,
                0L);
        Video savedVideo = vRep.save(video);
        String imageUrl = storageService.genLinkForUpload(genPreviewUrl(savedVideo.getId()));
        String videoUrl = storageService.genLinkForUpload(genVideoUrl(savedVideo.getId()));
        // TODO:
        // Добавить подтверждение загрузки клиентам и запускать поток,
        // который через 15 мин будет удалять видео, если загрузка не подтверджена
        return new UploadVideoResult(savedVideo.getId(), imageUrl, videoUrl);
    }

    public void deleteVideo(Long vId) {
        vRep.deleteById(vId);
        storageService.delete("/images/previews/" + vId);
        storageService.delete("/videos/" + vId);
    }

    @Transactional
    public VideoView getVideo(Long id) {
        Optional<Video> found = vRep.findById(id);
        if (found.isEmpty())
            return null;
        Video video = found.get();
        Long uId = (Long) request.getSession().getAttribute("userid");
        LikeEnum type = lServ.isLikedBy(video.getId(), uId);
        boolean isSubscribedAt = subscriptionRepository.findBySubscriberIdAndSubscribeeId(uId, video.getOwner().getId()).isPresent();
        String imageUrl = storageService.genLinkForDownload(genPreviewUrl(video.getId()));
        String videoUrl = storageService.genLinkForDownload(genVideoUrl(video.getId()));
        return new VideoView(
                video.getId(),
                video.getName(),
                video.getDescription(),
                video.getDuration(),
                video.getOwner().getId(),
                video.getOwner().getUsername(),
                video.getLikes(),
                video.getDislikes(),
                video.getComments(),
                LikeEnum.LIKE == type,
                LikeEnum.DISLIKE == type,
                video.getOwner().getSubscribers(),
                isSubscribedAt,
                imageUrl,
                videoUrl);
    }

    @Transactional
    public List<VideoView> getRandomVideosHeaders() {
        List<Video> videos = vRep.findAll(Sort.by("id").descending());
        List<VideoView> videoViews = new LinkedList<>();
        Long userId = (Long) request.getSession().getAttribute("userid");
        for (Video video : videos) {
            // TODO:
            // Remove N query
            LikeEnum type = lServ.isLikedBy(video.getId(), userId);
            boolean isSubscribedAt = subscriptionRepository.findBySubscriberIdAndSubscribeeId(userId, video.getOwner().getId()).isPresent();
            String imageUrl = storageService.genLinkForDownload(genPreviewUrl(video.getId()));
            String videoUrl = storageService.genLinkForDownload(genVideoUrl(video.getId()));
            videoViews.add(new VideoView(
                    video.getId(),
                    video.getName(),
                    video.getDescription(),
                    video.getDuration(),
                    video.getOwner().getId(),
                    video.getOwner().getUsername(),
                    null,
                    null,
                    null,
                    LikeEnum.LIKE == type,
                    LikeEnum.DISLIKE == type,
                    video.getOwner().getSubscribers(),
                    isSubscribedAt,
                    imageUrl,
                    videoUrl));
        }
        return videoViews;
    }

    public List<VideoView> searchVideos(String searchText) {
        if (searchText.isEmpty()) {
            return getRandomVideosHeaders();
        }
        List<VideoView> videoViews = vRep.searchVideos(searchText);
        for (VideoView videoView : videoViews) {
            String imageUrl = storageService.genLinkForDownload(genPreviewUrl(videoView.getId()));
            String videoUrl = storageService.genLinkForDownload(genVideoUrl(videoView.getId()));
            videoView.setImageUrl(imageUrl);
            videoView.setVideoUrl(videoUrl);
        }
        return videoViews;
    }
}
