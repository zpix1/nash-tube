package com.nashtube.video;

import com.nashtube.exceptions.FileParseFailedException;
import com.nashtube.video.views.VideoView;
import lombok.Getter;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@RequestMapping(path = "api")
public class VideoController {
    private static final String USER_ID_KEY = "userid";

    private final VideoService vServ;

    @Resource
    private HttpServletRequest request;

    public VideoController(VideoService vServ) {
        this.vServ = vServ;
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<?> handleDataIntegrityViolationException() {
        return ResponseEntity.status(400).body("Reason: possible constraint violation");
    }

    @ExceptionHandler(FileParseFailedException.class)
    public ResponseEntity<?> handleFileParseFailedException() {
        return ResponseEntity.status(400).body("Reason: could not parse video file received!");
    }

    @PostMapping(path = "/channel/{channelId}/videos/upload")
    public ResponseEntity<?> addVideo(
            @PathVariable("channelId") Long channelId,
            @Valid @RequestBody VideoUploadForm videoUploadForm) {
        if (request.getSession().getAttribute(USER_ID_KEY) == null)
            return ResponseEntity.status(401).build();
        if (request.getSession().getAttribute(USER_ID_KEY) != channelId)
            return ResponseEntity.status(403).build();
        VideoService.UploadVideoResult res = vServ.uploadVideo(
                channelId,
                videoUploadForm.getName(),
                videoUploadForm.getDescription(),
                videoUploadForm.getDuration());
        return ResponseEntity.ok().body(res);
    }

    @DeleteMapping(path = "/channel/{channelId}/videos/delete")
    public ResponseEntity<?> deleteVideo(@PathVariable("channelId") Long cId, @Valid @NotNull @RequestParam Long vId) {
        if (request.getSession().getAttribute(USER_ID_KEY) == null)
            return ResponseEntity.status(401).build();
        else if (request.getSession().getAttribute(USER_ID_KEY) != cId)
            return ResponseEntity.status(403).build();
        else {
            vServ.deleteVideo(vId);
            return ResponseEntity.ok().build();
        }
    }

    @ResponseBody
    @GetMapping(path = "videos/get/data", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getVideoHeader(@RequestParam @Valid @NotNull Long id) {
        VideoView res = vServ.getVideo(id);
        if (res == null)
            return ResponseEntity.badRequest().body("Reason: video with such id does not exist");
        return ResponseEntity.ok().body(res);
    }

    @ResponseBody
    @GetMapping(path = "videos/get/random", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getVideosHeaders() {
        List<VideoView> videoViews = vServ.getRandomVideosHeaders();
        return ResponseEntity.ok().body(videoViews);
    }

    @GetMapping(path = "videos/search")
    public ResponseEntity<?> searchVideo(@RequestParam String query) {
        List<VideoView> videos = vServ.searchVideos(query);
        return ResponseEntity.ok().body(videos);
    }

    @Getter
    public static class VideoUploadForm {
        @NotBlank
        @NotNull
        private String name;
        @NotBlank
        @NotNull
        private String description;
        @NotNull
        private Long duration;
    }
}

