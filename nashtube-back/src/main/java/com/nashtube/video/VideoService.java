package com.nashtube.video;

import com.nashtube.video.views.VideoView;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public interface VideoService {

    UploadVideoResult uploadVideo(Long channelId, String name, String description, Long duration);

    List<VideoView> getRandomVideosHeaders();

    VideoView getVideo(Long id);

    void deleteVideo(Long vId);

    List<VideoView> searchVideos(String searchText);

    String genPreviewUrl(Long previewId);

    String genVideoUrl(Long videoId);

    @AllArgsConstructor
    @Getter
    @Setter
    class UploadVideoResult {
        private Long id;
        private String imageUrl;
        private String videoUrl;
    }
}
