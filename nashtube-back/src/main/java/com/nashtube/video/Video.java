package com.nashtube.video;

import com.nashtube.user.NashUser;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "nash_video")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Generated
public class Video {

    @Id
    @SequenceGenerator(name = "videoSequence", sequenceName = "video_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "videoSequence")
    private Long id;
    @Column(name = "inserted_at", nullable = false)
    @CreationTimestamp
    private Timestamp insertedAt;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description", columnDefinition="TEXT")
    private String description;
    @Column(name = "duration", nullable = false)
    private Long duration;
    @ManyToOne(targetEntity = NashUser.class, fetch = FetchType.EAGER)
    @JoinColumn(
            referencedColumnName = "id",
            nullable = false,
            foreignKey = @ForeignKey(name = "videoOwnerForeignKeyConstraint")
    )
    private NashUser owner;
    @Column(nullable = false)
    private Long likes;
    @Column(nullable = false)
    private Long dislikes;
    @Column(nullable = false)
    private Long comments;
}
