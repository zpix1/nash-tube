package com.nashtube.video;

import com.nashtube.video.views.VideoView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface VideoRepository extends JpaRepository<Video, Long> {
    @Query("update Video v set v.likes = v.likes + 1 where v.id = ?1")
    @Modifying
    void incrementLikesOf(Long vId);

    @Query("update Video v set v.likes = v.likes - 1 where v.id = ?1")
    @Modifying
    void decrementLikesOf(Long vId);

    @Query("update Video v set v.dislikes = v.dislikes + 1 where v.id = ?1")
    @Modifying
    void incrementDislikesOf(Long vId);

    @Query("update Video v set v.dislikes = v.dislikes - 1 where v.id = ?1")
    @Modifying
    void decrementDislikesOf(Long vId);

    @Query("select v from Video v where v.owner.id in ?1")
    List<Video> getVideosBySetOfOwnersIds(List<Long> ownersIds);

    @Transactional
    @Query("""
            SELECT
                NEW com.nashtube.video.views.VideoView(nv.id, nv.name, nv.duration)
            FROM Video nv
            WHERE nv.owner.id = :user_id
            ORDER BY nv.insertedAt DESC NULLS LAST
            """)
    List<VideoView> getVideosForUserChannel(@Param("user_id") Long user_id);

    @Query("""
            SELECT NEW com.nashtube.video.views.VideoView(v.id, v.duration, v.name, owner.id, owner.username)
            FROM Video v
            JOIN v.owner owner
            WHERE fts(:search_text)=true
            ORDER BY ftsc(:search_text)
            """)
    List<VideoView> searchVideos(@Param("search_text") String searchText);
}
