create table subscribtion (
      id int8 not null,
      subscribee int8,
      subscriber int8,
      primary key (id)
);

create sequence subscribtion_sequence start 1 increment 1;

alter table subscribtion
    add constraint SubscriberSubscribeeUniqueConstraint unique (subscriber, subscribee);

alter table subscribtion
    add constraint subscribeeForeignKeyConstraint
        foreign key (subscribee)
            references nash_user;

alter table subscribtion
    add constraint subscriberForeignKeyConstraint
        foreign key (subscriber)
            references nash_user;

alter table nash_user
    add column subscribers int8;

update nash_user
set subscribers = 0;

alter table nash_user
    alter column subscribers set not null;
