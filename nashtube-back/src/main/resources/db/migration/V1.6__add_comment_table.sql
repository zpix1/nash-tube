create table comment (
     id int8 not null,
     comment_date timestamp not null,
     comment_text varchar(255) not null,
     comment_from_id int8 not null,
     comment_on_id int8 not null,
     primary key (id)
);

create sequence comment_sequence start 1 increment 1;

alter table comment
    add constraint userCommentForeignKey
        foreign key (comment_from_id)
            references nash_user;

alter table comment
    add constraint videoCommentForeignKey
        foreign key (comment_on_id)
            references nash_video;