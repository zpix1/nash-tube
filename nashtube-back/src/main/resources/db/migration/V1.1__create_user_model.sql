create table nash_user (
   id int8 not null,
   password varchar(255) not null,
   username varchar(255) not null,
   primary key (id)
);

alter table nash_user
    add constraint UserUsernameUniqueConstraint unique (username);

create sequence nash_user_sequence start 1 increment 1
