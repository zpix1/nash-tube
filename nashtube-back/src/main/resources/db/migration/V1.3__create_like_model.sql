create table like_entity (
     id int8 not null,
     type varchar(255),
     like_from int8,
     like_to int8,
     primary key (id)
);

create sequence like_sequence start 1 increment 1;

alter table like_entity
    add constraint LikeVideoUserUniqueConstraint unique (like_from, like_to);

alter table like_entity
    add constraint LikeFromForeignKeyConstraint
        foreign key (like_from)
            references nash_user;

alter table like_entity
    add constraint LikeToForeignKeyConstraint
        foreign key (like_to)
            references nash_video;

alter table nash_video
    add column likes int8;

alter table nash_video
    add column dislikes int8;

alter table nash_video
    add column comments int8;

update nash_video
    set likes = 0, dislikes = 0, comments = 0;

alter table nash_video
    alter column likes set not null;

alter table nash_video
    alter column dislikes set not null;

alter table nash_video
    alter column comments set not null;
