create table nash_video (
   id int8 not null,
   description varchar(255),
   duration int8 not null,
   nash_image oid not null,
   name varchar(255) not null,
   nash_video oid not null,
   owner_id int8 not null,
   primary key (id)
);

alter table nash_video
    add constraint videoOwnerForeignKeyConstraint
        foreign key (owner_id)
            references nash_user;

create sequence video_sequence start 1 increment 1;


