if [[ ${ENV} = "PROD" ]]
then
    ./mvnw -Ptest -Psonar install
else
    ./mvnw -Pdev install
fi